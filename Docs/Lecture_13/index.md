# Lecture 13: The Flat Norm Saga: from Image Denoising and Shape Recognition through Properties of Minimizers to Median Shapes and Chemical Interfaces

```{figure} Figures/example_figure.png
---
height: 350px
name: ex_fig
---
This is a caption of an example figure
```

Our tale begins in 2006 at Institute for Math and its Applications (IMA). There was a thematic year at the host school on image analysis and processing. Kevin had gone for 6 weeks when he met Simon Morgan, a student of Bob Harts, who was there as an instructor. Kevin and he talked about Geometric Measure Theory and there was a workshop which focused on variational methods in image analysis. Fields medalist Algebraic Geometer David Mumford gave a talk on diffeomorphic maps between shapes and he was after a distance measure in this space. As an offhand remark, he mentioned the flat norm as a possible candidate. Just after the talk, Kevin and Simon went to a conference room to discuss the idea.

They'd known about the $L^1TV$ functional: 
```{margin}
Bigger $\lambda$ gets us closer to the data, but less smooth, and vice versa.
```

````{prf:definition}
:label: L1TV_Functional

For $u,f\in L^1(U,\mathbb{R}),$ $f$ being our data, the **$L^TV$ Functional** is defined by
\begin{gather*}
	F(u) \equiv \int_\Omega|\nabla u|\, dx + \lambda \int_\Omega |u-f|\, dx.
\end{gather*}

(Minima of the $L^1TV$ are used for smoothing images.)

````


Selim Esedoglu and Stan Osher wrote a paper discussing this functional and showed that if $f = \chi_\Sigma,$ 
:::{sidebar} Characteristic Function

A **characteristic function** is 1 on a set and 0 off of that set:

\begin{gather*}
  \chi_E(x) \equiv \begin{cases}
    1, \textrm{ if } x\in E\\
    0, \textrm{ otherwise}
  \end{cases}
\end{gather*}

:::

some characteristic function, then
\begin{gather*}
	u^\dagger = \textrm{argmin}_{u} F(u)
\end{gather*}
such that $u^\dagger = \chi_{E^\dagger}$. Further,

:::{sidebar} Symetric Difference

The funky notation used here is the **symmetric difference**:
\begin{gather*}
  E\triangle \Sigma = (E - \Sigma) \cup (\Sigma- E)
\end{gather*}
I.e., the points that are in one or the other set, but not in both sets. Like a Venn diagram without the eyeshape in the middle.

:::

\begin{gather*}
	\mathscr{F}(E) = \int_\Omega | \nabla\chi_E |
\, dx + \lambda\int_\Omega|\chi_{E\triangle\Sigma}|\, dx.
\end{gather*}
This fist term is infinite, but we can interpret it using the theory of sets of finite perimeter to get
\begin{gather*}
	\mathscr{F}(E) = \textrm{Per}(E) + \lambda\ |E\Delta\Sigma|.
\end{gather*}
(*figure of red dots*)

Simon and Kevin noticed that this was just the flat norm when thought of as a functional on the boundary of sets, rather than as a functional on sets. Recall the flat norm:

````{prf:definition}
:label: Flat_Norm

The *Flat Norm* is defined by
\begin{gather*}
	F_\lambda(T) = \min_S(M(T-\partial S) + \lambda\ M(S))
\end{gather*}
````

(*Figure of two currents*)

We take 
\begin{gather*}
	T_1 - T_2 = T_1 - T_2 - \partial S + \partial S
\end{gather*}
where $T_1,T_2$ are currents and $S$ is a set.
Next, we measure $T_1 - T_2 - \partial S$ the usual way, then measure $S$ instead of its boundary, $\partial S$:
\begin{gather*}
	M(T_1 - T_2 - \partial S) + \lambda M(S)
\end{gather*}

(*Figure of bean sigma in yellow E in red*)

Now we notice that if $E = T - \partial S$ and $E\Delta\Sigma = S$, then the Flat Norm is

\begin{gather*}
	\partial \Sigma - \partial(E\Delta\Sigma) = \partial E.
\end{gather*}

 Then, we make the substitution $\partial \Sigma \to T$ and $E\triangle\Sigma \to S$, we have 
 
\begin{gather*}
	\mathscr{F}_\lambda(E) = F_\lambda(E).
\end{gather*}

So, the $L^1TV$ computes the flat norm for boundaries.

## Application: Average of a Set of Shapes
We want a way of finding the average of a set of shapes.

One can take the mean to be:
\begin{gather*}
	\min_S\sum_i|S - S_i|^2
\end{gather*}
and the median as:
\begin{gather*}
	\min_S\sum_i |S - S_i|
\end{gather*}
for some distance $|\cdot|$. The Flat Norm is a perfect candidate here. Out of this came three papers:

1. [Simplicial Deformation paper](https://arxiv.org/abs/1105.5104)
2. [Integral in $\implies$ integral out](https://arxiv.org/abs/1411.0882)
3. [Median shapes](https://arxiv.org/abs/1802.04968)

The next stage was [this paper](https://pubs.acs.org/doi/abs/10.1021/acs.jctc.0c00260) wherein the Flat Norm is used to identify protrusions in soft matter interfaces.



