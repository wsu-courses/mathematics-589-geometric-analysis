# Lecture 1: Geometric Linear Algebra
### (Linear Algebra Done Right According to a Geometric Analyst)
---

![](figures/plane_intersection.png)

In this chapter we will lay the linear algebra framework necessary to understand geometric analysis. We presume the reader is already familiar with linear algebra and calculus as one would find in their standard undergraduate courses. However, we will review certain key concepts from the geometric analyst's perspective both to refresh the concepts and to provide geometric insight.

## Vector spaces

Vector spaces are essential objects of study in both modern math and physics.

Many students are introduced to vectors simply as arrows that may be moved around the plane and added tip to tale or have their length scaled by multiplication of some real number.

The mathematician sees vectors more generally than this. Any mathematical object that behaves like we would expect said arrows to behave will qualify as a vector. A precise definition follows:

````{prf:definition}
:label: vector_sapce

A **vector space** is a triple $(V,+,\cdot)$ consisting of a set $V$, a function $+$ (called vector addition) and a function $\cdot$ (called scalar multiplication). 

The elements of $V$ are informally called **vectors**. 

Vector addition takes two vectors from $V$ and returns a vector in $V$. For shorthand we will use function notation to mean the same thing $+: V \times V \to V$. Scalar multiplication takes a real number and a vector and scales the latter by the former, $\cdot: \mathbb{R} \times V \to V$. (It may be noted that for even more generality scalar multiplication can be done with any field and not just real numbers).

These two functions must behave like we would expect given our background in addition and multiplication. That is, they must satisfy the following axioms:

Let $v,w,u \in V$ and let $\lambda, \mu \in \mathbb{R}$.

\begin{gather*}
v + w = w + v\\
(u + v) + w = u + (v + w)\\
\text{There exists } 0 \in V \text{ such that for all } v \in V: v + 0 = v\\
\text{ For all } v \in V: \text{ there exists } (-v) \in v : v + (-v) = 0\\
\lambda \cdot (\mu \cdot v) = (\lambda \cdot \mu) \cdot v\\
(\lambda + \mu) \cdot v = \lambda \cdot v + \mu \cdot v\\
\lambda \cdot v + \lambda \cdot w = \lambda\ \cdot (v + w)\\
1 \cdot v = v
\end{gather*}

````

```{margin}
Although $+$ and $\cdot$ are functions, we suppress function notation for ease of use and instead write $+(v,w) = v + w$ and $\cdot(\lambda,v) = \lambda \cdot v = \lambda v$
```

If the vector addition and scalar multiplication are understood from context we may simply refer to the set underlying the vector space as the vector space itself. For example, if we simply refer to $\mathbb{R}$ as a vector space, you should presume the ordinary gradeschool definitions of addition and multiplication.

## Linear maps and subspaces

A "space" in mathematics is a set equipped with some additional structure. It is a common recurring theme in mathematics to study spaces by examining the structure preserving maps between them.

```{margin}
A map is another word for function. We prefer to use the term "map" when we're thinking geometrically but they may be used interchangeably.
```

The structure preserving maps of vector spaces are linear functions. By structure preserving we mean the following:

If we have a linear function with a domain $D$ (thought of as a big bag of vectors) and we take each vector $d \in D$ in that bag and map it with our function $f(d)$ and put all the results in a big bag $\{ f(d) | d \in D \}$ this collection forms a natural vector space in its own right.

We will now give a precise definition:

````{prf:definition}
:label: linear

Let $V$ and $W$ be vector spaces. A map $f: V \to W$ is called **linear** if for all $x, y \in V$ and $c \in \mathbb{R}$:

\begin{gather*}
f(cx + y) = cf(x) + f(y)
\end{gather*}
````
Put simply, a function is linear if it splits over addition and constant multiples can be factored out. Derivatives and integrals are examples of linear functions.

```{margin}
Stop and think: What vector spaces are derivatives mapping from and to?
```

Closely related to linear functions are *affine* functions which are, in a sense, *almost* linear.

````{prf:definition}
:label: affine

Let $V$ and $W$ be vector spaces. A map $f: V \to W$ is called **affine** if:

\begin{gather*}
g(x) := f(x)-f(0)
\end{gather*}

Is a linear map.
````

Affine maps behave like linear maps, up to a constant shift of the origin.

```{margin}
Show that the general equation of a line $f(x) = mx + b$ is not linear but is affine.
```

Often when working with a vector space we are interested in some subset rather than the entire ambient space - perhaps we are looking for solutions to some problem and we can narrow our search to only those vectors satisfying some property. This is where subspaces come into play:

````{prf:definition}
:label: linear_subspace

A subset $W \subseteq V$ of a vector space $V$ that is a vector space in its own right is called a **subspace** (of $V$).
````

Not only is it often more convenient and practical to work in subspaces but subspaces can also provide key insights into the nature of a problem.

To prove a set equipped with a $+$ and $\cdot$ forms a vector space requires us to show the 9 properties from earlier hold. However, if we wish to show a subset $W \subseteq V$ of a vector space is a subspace we need only show 3 things:

1. The zero vector 0 is in $W$.
2. The sum $u + v$ of two elements $u,v \in W$ in $W$ is also in $W$.
3. The scaled vector $u \in W$ in $W$ we get by multiplying by a real number $\lambda \in \mathbb{R}$, $\lambda \cdot u$ is still in $W$.

The reason why we need to prove fewer things (than we need to prove to show a set is a vector space) to show a subset is a vector space is that by assuming the parent set is already a vector space we know that $+$ and $\cdot$ already satisfy the properties we need.

If we are given two subspaces of some vector space $V$ we can also look at the vectors that exist in both:

````{prf:theorem} Orthogonal-Projection-Theorem
:label: my-theorem

The intersection of subspaces is a subspace.

If $U,W \subseteq V$ are subspaces of $V$ then $U \cap W = \{ x | x \in U \text{ and } x \in W \}$ is a subspace of $V$.

The proof is left as an exercise to the reader.
````

This proves to be useful in narrowing in on solutions by further restricting our points of interest.

Additionally we include a subspace analog of affine maps:

````{prf:definition}
:label: affine_subspace

A subset $M$ of a vector space $V$ that can be expressed as a pointwise sum of a linear subspace $W$ and some element $v \in V$:

\begin{gather*}
M = \{ w + v | w \in W \}
\end{gather*}

Is called an affine subspace (of $V$).
````

Which are often the next best thing to work in. We may think of these as sets that, if shifted would include the origin and then act as vector spaces.

## Matricies, geometrically

Matricies are often introduced as a way to represent a system of linear equations by collecting all the coefficients, the variables to be solved for, and writing them as column vectors sometimes stitched together to make a rectangle of numbers.

We will think about matricies a little bit differently. 

Recall the notion of span and linear independence.

The **span** of a set of vectors $A = \{v_1, v_2, ..., v_k \}$ is the set of all possible linear combinations of those vectors. That is:

\begin{gather*}
\text{span}(A) = \{ \sum_{i=1}^k a_i v_i | a_i \in \mathbb{R} \}
\end{gather*}

A collection of vectors $\{v_1, v_2, ..., v_k \}$ is **linearly independent** if no vector lies in the span of the other vectors.

A **basis** for a vector space $V$ is a subset $A \subseteq V$ of $V$ that is linearly independent and $\text{span}(A) = V$.

Bases are a way of representing vector spaces with a smaller collection of vectors within the space. Generally there are many possible bases we could choose from to represent a given vector space, so we should chose whatever basis makes our calculations easier. The number of vectors in a given basis for a vector space $V$ is called the dimension of $V$ often written $\text{dim}(V)$ - all bases for a given $V$ have the same number of elements so it is well-defined.

Bases for finite dimensional vector spaces are extraordinarily useful in their ability to help us perform calculations.

Suppose we have a vector space $V$ and a basis $A = \{v_i\}_{i=1}^k$ for $V$. Then for each element $v \in V$ we can write:

\begin{gather*}
v = a_1 v_1 + a_2 v_2 + \cdots + a_k v_k
\end{gather*}

If we take all the coefficients $\{ a_i\}_{i=1}^k$ and write them in a column we can represent $v$ as a simple column of numbers:

\begin{gather*}
v = \begin{bmatrix} a_1\\
a_2\\
\vdots\\
a_k
\end{bmatrix}
\end{gather*}

This is more powerful than it might appear on first impression; the above implies that at least for finite dimensional vector spaces (which may be very abstract) we can work with the vectors as simple columns of numbers like we are likely used to working with.

Furthermore bases unlock for us one of the most important insights in linear algebra:

````{prf:theorem}
:label: my-theorem

Let $V$ be an $n$-dimensional vector space and $W$ be an $m$-dimensional vector space. Then any linear map $f: V \to W$ can be represented by matrix multiplication:

\begin{gather*}
f(x) = Ax
\end{gather*}

Where $A$ is a $m \times n$ matrix and $x$ is a $1\times n$ column vector representing the element in $V$ to be mapped w.r.t some bases established for both $V$ and $W$.

Furthermore, any $m \times n$ matrix $A$ represents some linear transformation $g: V \to W$.
````

Any time we speak of a linear map we can think about or work with its guarenteed matrix representation, and any time we speak of a matrix we can think about or work with its guarenteed corresponding linear map. Hence from now on, we may use them interchangeably. If we were performing concrete calculations (which often depend on bases of our choosing) we would need to specify a basis for the domain and range of our linear map; however when we speak abstractly it is good enough to know that some basis always exists for the spaces in which we work - which we presume going forward is true.

When we are thinking about matricies geometrically we are primarily concerned with 3 important subspaces that a given matrix defines: the Null Space, Column Space, and Row Space.

````{prf:definition}
:label: Null space

The nullspace of a matrix $A$ is defined to be the set of all column vectors $x$:

\begin{gather*}
\text{null}(A) := \{ x : Ax = 0 \}
\end{gather*}

that map to 0 when multiplied on the left by $A$. If we think of $f(x) = Ax$ as a function with a domain $D$ then these are all those vectors that get squished onto the origin when we push forward the domain to get $f(D)$.

````

````{prf:definition}
:label: Column space

The column space of a matrix $A$ is defined to be the span of the columns of $A$. If:

\begin{gather*}A =
\begin{bmatrix}
      \vert & \vert & \vert & \vert \\
      c_1 & c_2 & \cdots & c_n \\
      \vert & \vert & \vert & \vert \\
    \end{bmatrix}
\end{gather*}

Then:

\begin{gather*}
\text{row}(A) = \left\{ \sum_{i=1}^n a_i c_i \right\}
\end{gather*}

Where $c_i$ are scalars.

````

````{prf:definition}
:label: Row space

The row space of a matrix $A$ is defined to be the span of the rows of $A$. If:

\begin{gather*}A = \begin{bmatrix}
      - & r_1 & -\\
      - & r_2 & -\\
      - & r_3 & -\\
\end{bmatrix}
\end{gather*}

Then:

\begin{gather*}
\text{col}(A) = \left\{ \sum_{i=1}^n a_i r_i \right\}
\end{gather*}

Where $c_i$ are scalars.

````

## Orthogonal projections and matricies

An inner product $\langle \cdot, \cdot \rangle$ is a generalization of the dot product that behaves in much the same way. If $v,w$ are vectors then we say $v$ and $w$ are **orthogonal** if $\langle v, w \rangle = 0$. Furthermore we can measure the angle $\theta$ between $v,w$ by:

\begin{gather*}
\theta := \arccos \left( \frac{\langle v,w}{|v||w|} \right)
\end{gather*}

A set of vectors $\{v_i \}_{i=1}^k$ is orthogonal if every vector in the collection is orthogonal to every other vector in the collection. 

Suppose we have such a set and construct the matrix $V$ whose columns are said vectors.

\begin{gather*}
V = \begin{bmatrix}
      \vert & \vert & \vert & \vert \\
      v_1 & v_2 & \cdots & v_k \\
      \vert & \vert & \vert & \vert \\
    \end{bmatrix}
\end{gather*}

Then we define the **orthogonal projection** onto $V$ $P_v$ to be:

\begin{gather*}
P_v(x) := VV^Tx
\end{gather*}

The orthogonal projecton of a particular vector $x$ onto $V$ is the closest point in the span of $\{v_i\}_{i=1}^k$ to $x$. 

TODO:
L^2 distance

<!-- ## Things that come up when considering linear algebra
The following ideas are present most often in problems that involve linear algebra: orthogonality, orthogonal matrices, subspaces, projections, QR decomposition and SVD, and determinants as volumes. We also encounter two primary kinds of vector spaces: Banach and Hilbert. 

## Banach vs Hilbert
Two very important spaces in linear algebra are the Banach and Hilbert spaces.

Banach spaces are characterized as complete vector spaces with a norm. A Hilbert space is a Banach space that's been equipped with an inner product which relates to the norm by
\begin{gather*}
	||x|| = \sqrt{\braket{x,y}}.
\end{gather*}
The inner product gives us the ability to measure angle between vectors in our space, while the norm gives us a notion of distance.
-->

## Determinants, condition numbers, and inverses
Consider a matrix $M$ and imagine taking all of the points of a cube $Q$ and pushing it through the matrix. The sides of the cube will get stretched and rotated with its vertices remaining connected while the angle between each edge might change as well. Since we're stretching and rotating we can consider asking "How much will the volume change after undergoing the transformation." The answer is surprisingly simple:
\begin{gather*}
	\textrm{Volume}\{M(Q)\} = \det(M)\cdot \textrm{Volume}\{Q\}.
\end{gather*}
In other words, the volume of $Q$ will change as the product of the determinate of $M$.

This is relationship holds for any shape:

(*figure of ellipse before and after a linear transformation*)

Notice, however, that when $\det(M) < 0$ this implies that negative volume is a reasonable concept. In reality, this negative determinant notifies us that the image of the shape will be reflected in some way. So, more accurately, our relationship is
\begin{gather*}
	\textrm{Volume}\{M(Q)\} = |\det(M)|\cdot \textrm{Volume}\{Q\}.
\end{gather*}

Given any particular matrix $A$ we can compute its **condition number** which tells us how sensitive the output $b$ of a matrix is to changes in the input $x$ in the equation $Ax = b$. We are generally concerned with inverting a matrix, and the condition number tells us how well we can do that.

Given a matrix $A$ its condition number $\kappa$ (for inversion) is given by:

\begin{gather*}
\kappa = \frac{\max_x \frac{\|Ax\|}{\|x\|}}{\min_x \frac{\|Ax\|}{\|x\|}}
\end{gather*}

Which is the ratio of the largest stretch the matrix $A$ can do on a vector to the smallest shrink it can do to a vector. 

The larger the condition number is the worse for us; bad conditions numbers come from matricies whose columns are nearly dependent.

### Orthogonal Matrices
An orthogonal matrix $M$ is one who's rows are orthogonal to one another, likewise for its columns. This gives us the nice relationship:
\begin{gather*}
	M^T = M^{-1}
\end{gather*}
i.e. the inverse of $M$ is the same as its transpose. Orthogonality can be quickly checked by the following relationship
\begin{gather*}
	M^T M = M M^T = I	
\end{gather*}
where $I$ is the identity. 

Taking the determinant of both sides, we have
\begin{gather*}
	\det(I) = \det(M^T M) = \det(M)\det(M^T) = (\det(M))^2
\end{gather*}
But, since $\det(I) = 1$, this means that an orthogonal matrix will at most reflect or rotate a shape after it's pushed through; no volume changes will occur.

## QR Decomposition
We can split any $m\times n$ matrix $A$ where $m\geq n$ into two matrices: $m\times m$ matrix $Q$ and $m\times n$ $R$, where $Q$ is unitary (meaning $Q Q^T = Q^T Q = Q Q^{-1} = I$) and $R$ is upper triangular. 

$Q$ being unitary has similar benefits to orthogonality:
\begin{gather*}
	QQ^T = I \implies (\det(Q))^2 = 1
\end{gather*}
In other words, $Q$ will at most rotate or reflect objects pushed through it. 

So, all matrices can be split into two steps: a possible reflection and rotation through $Q$ and a stretching (with possibly more reflection and rotation) from $R$.

(*Ellipse being being pushed through $Q$ then $R$*)

## Singular Value Decomposition (SVD)

Similarly to the QR decomposition, the SVD splits an $m\times n$ matrix $M$ into stretches and rotations:

\begin{gather*}
  M_{m\times n} = \begin{bmatrix}
      \vert & \vert & \vert & \vert \\
      u_1 & u_2 & \cdots & u_n \\
      \vert & \vert & \vert & \vert \\
    \end{bmatrix}
    \begin{bmatrix}
      \sigma_1 & 0 & \cdots & 0 & 0 & \cdots & 0\\
      0 & \sigma_2 & \cdots & 0 & 0 & \cdots & 0\\
      \vdots & \ddots & \ddots & \vdots & \vdots & \ddots & \vdots \\
      0 & \cdots & \cdots & \sigma_n & 0 & \cdots & 0
    \end{bmatrix}
    \begin{bmatrix}
      \textrm{---} & v_1 & \textrm{---} \\
      \textrm{---} & v_2 & \textrm{---} \\
      \textrm{---} & \vdots & \textrm{---} \\
      \textrm{---} & v_m & \textrm{---} \\
    \end{bmatrix} 
    = U_{m\times n}\Sigma_{n\times m} V_{n\times m}^T
\end{gather*}

were $U$ and $V^T$ are unitary and $\Sigma$ is positive definite. $U$ and $V^T$ will contain the rotational and flipping information of $M$, while $\Sigma$ contains all of the stretching information, and nothing else! This means that we have
\begin{gather*}
	|\det(M)| = |\det(\Sigma)|
\end{gather*}
which can be easily verified.

(*Figure of rotate/flip, stretch, rotate/flip of SVD*)

## Eigenstuff

Given a square matrix $A$ we can compute its **spectrum** which is the set of all of its eigenvalues or, more generally the set of all scalars $\lambda$ such that $A-\lambda I$ is not invertible.

The spectrum of a matrix provides useful information about the matrix and its associated linear transformation.

The **adjoint** of a matrix $A$ denoted $A^*$ is the conjugate-transpose of $A$ and satisfies:

\begin{gather*}
\langle Av, w \rangle = \langle v, A^* w \rangle
\end{gather*}

A self-adjoint matrix is one in which the matrix is its own adjoint, i.e.:

\begin{gather*}
A = A^*
\end{gather*}

Which are extremely nice to work with.

A matrix is called a **Normal Matrix** if:

\begin{gather*}
AA^* = A^*A
\end{gather*}

And is similarly nice to work with.