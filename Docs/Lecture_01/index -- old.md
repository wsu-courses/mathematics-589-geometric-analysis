# Lecture 1: Geometric Linear Algebra

The Singular Value Decomposition

\begin{gather*}
   A = U\Sigma V^T
\end{gather*}

