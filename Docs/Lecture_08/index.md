<!-- (Point For Week 8: tbd) -->

# Lecture 8: Degree Theory and Fixed Point Theorems

![](Figures/idea.png)

```{figure} Figures/example_figure.png
---
height: 350px
name: ex_fig
---
This is a caption of an example figure
```

:::{sidebar} Example Topic

This is an example of a sidebar.
:::

```{margin}
This is a margin note.
```

Degree theory is a tool to study things that are invarient under continuous maps and largely finds its application in nonlinear analysis (especially optimization). To begin with, we'll focus on the 1D case.

For example, it can be used to show Browers fixed point theorem, which demonstrates that all homeomorphic  maps from the unit ball to itself have at least 1 fixed point. This fact has been shown in 250+ ways, but here we'll show it using Degree Theory.


(*Figure of the map from the unit disc to itself*)

---

## Homeomorphic and Diffeomorphic maps

Before we get into the weeds of degree theory, we need a few tools.

````{prf:definition}
A map $f: X\to Y$ is **homeomorphic** if
1. $f$ is one-to-one and onto
2. $f$ and $f^{-1}$ is continuous
````

````{prf:definition}
A map $f$**diffeomorphic** if
1. homeomorphic
2. $Df$ and $Df^{-1}$ are non-singular everywhere
````

There's an analogue for analyic maps, but you lose the ability to cut things apart using bump functions.

Bump functions are nice, since we can take a nasty function and convolve it with a bump function with width $\epsilon$ whose integral is 1 to smooth out the nasty points. This acts as a generalized average (a convex combination) around the nasty point. Then, taking $\epsilon\to 0$, it will converge to the original function. We can do this as long as our function is integrable. This is called mullification and it allows you to make any function $C^\infty$.

---

Back to fixed point theorems.

## Brouwer's Fixed Point Theorm


````{prf:theorem}
**Brouwer's Fixed Point Theorem**: Every homeomorphic map $f:B(0,1)\to B(0,1)$ has a fixed point.
````

Recall that a function $f$ is differentiable at some point $x$ in its domain if there exists a matrix $f'(x)$ such that $f(x+h) = f(x) + f'(x)h+o(h)$ where $\frac{|o(h)|}{|h|} \to 0$ as $|h| \to 0$. If $f$ is differentiable at $x$ then $J_f(x) = \det f'(x)$ is defined to be the Jacobian of $f$ at $x$. If $J_f(x) = 0$ then $x$ is a critical point of $f$ and if $f^{-1}(y)$ contains no critical points then $y$ is a regular value of $f$. If $y$ is not a regular value then it is by definition a singular value of $f$.

We want to fiddle around with the following idea: consider a function supported on the interval $[0,1]$ in $1D$ and consider its regular values.

(*Figure of the regular values of an arbitrary function*)

Regular Value: $y$ in range is a *regular value* if $Df$ is full rank at every point in $f^{-1}(y)$.

For the $1D$ case, $Df$ is full rank as long as it's nonzero.

````{prf:definition}
:label: my-definition

Let $\Omega \subset \mathbb{R}^n$ be an open bounded set and $f \in C^1(\bar{\Omega})$. If the point $y$ is not on the image of the boundary of $\Omega, y \not \in f(\partial \Omega)$ and $y$ is a regular value of $f$, $J_f(y) \neq 0$ then we define the **Brouwer degree** denoted by $\text{deg}(f,\Omega,y)$ to be:

\begin{gather*}
\text{deg}(f,\Omega,y) = \sum_{x \in f^{-1}(y)}\text{sign} J_f(x)
\end{gather*}

Noting that if $f^{-1}(y) = \emptyset$ then the degree is 0.
````

```{margin}
Recall that $\bar\Omega$ is the closure $\Omega \cup \partial \Omega$.
```

Note that in our case this is equivalent to:

\begin{gather*}
	\deg(f,\Omega, y) = \sum_{x\in f^{-1}(y)} \textrm{sign}(f'(x))
\end{gather*}

````{prf:example}
Prove that if $f:[0,1]\to\mathbb{R}$ is $C^1$ then $f^{-1}(y)$ for regular $y$ is finite.
````

This exercise shows our definition of degree is always finite.

We proceed as follows:

1. Find a $C^{2}$ function $g$ sufficiently close to $f$.

2. Note that the degree of $f$ is the same as the degree of $g$ at its regular points.

Notice, between our boundary points, the degree is $1$, and on either side it's $0$.

Notice that $\deg(f,(0,1),y) = 1$ and $\deg(f,\{0,1\},y) = 0.$

The degree will always be 1 on the interior but may differ on the boundary; hence we at most need to divide the range into 3 intervals.

Back to our proof...

```{margin}
See *Nonlinear Functional Analysis* by Deimling for the definition and properties of degree.
```
4. Define degree for continuous through close $C^2$ functions .

	 1. Note, $\deg(f, \Omega, y) \neq 0$ implies $f^{-1}(y)\neq \emptyset$

For the rest of the proof, consider the following idea. Consider the closed unit ball $B$. 
1. Note, there is no continuous $h:B\to\partial B$ such that $h(x) = x$ when $x\in\partial B$. 
2. Suppose $g:B\to B$ and $g$ is continuous and $g$ has no fixed point.
3. Define $f:B\to\partial B$ by the following picture:
(*figure of ball mapping (Kevin calls it $g$ not $f$)*)
In other words, $g(x)$ is the point acquired by projecting the ray through $x$ and $g(x)$ onto the boundary. 
	1. This is a contradiction, since $f$ is a continuous map and $f(x) = x$ when $x\in\partial B$
	2. Take $g:B\to\partial B$ and $I:B\to \partial B$ (identity), then
	\begin{gather*}
		(1-t)I(x) + tg(x):B\to \mathbb{R}^n
	\end{gather*}
	is a homotopy that can be used to show 1. above

### Application
Take a [markov matrix](https://en.wikipedia.org/wiki/Examples_of_Markov_chains#:~:text=A%20game%20of%20snakes%20and,memory'%20of%20the%20past%20moves.):
\begin{gather*}
  P = \begin{pmatrix}
    P_{11} & P_{12} & \cdots & P_{1N} \\
    P_{21} & P_{22} & \cdots & P_{2N} \\
    \vdots & \vdots & \ddots & \vdots \\
    P_{N1} & P_{N2} & \cdots & P_{NN}
  \end{pmatrix}
\end{gather*}

This matrix has at least one eigenvecctor with eigenvalue $1$, so it's fixed.

One way to think about this is that all of the $\rho_i$ and the $\{p_i\}$ are in $\mathbb{R}^n$. I.e. they are the standard probability simplex in $\mathbb{R}^n$. This is homeomorphic to the ball. So, there must be a fixed point by Brouwer's fixed point theorem.

## Banach Fixed Point Theorem
````{prf:theorem}
1. $F:X\to X$ where $X$ is a Banach space
(Reminder: Banach spaces are complete normed linear spaces) 
2. $|F(x) - F(y)| \leq K |x-y$| with $0\leq k < 1$ (i.e., $F$ is a contraction mapping)
3. We can conclude that there exists $x^*\in X$ such that $F(x^*) = x^*$ 
````

In other words, if you have a map from a space onto itself that shrinks space, then there must be a fixed point.

Now, usually, when we want to solve an equation, we have $F(x) = c$. we can solve this using fixed point methods by the following
\begin{gather*}
	F(x) = c \implies F(x) - c = 0 \implies F(x) - c + x = x
\end{gather*}
then we define $g(x) = F(x) - c + x$ and we can use fixed point methods to solve the original problem.

Now, consider the following example: $x = y$.
And take a function $F$ who is always above $x=y$.
(*figure of this*)
But then, a line with slope $K$ must cross $y=x$ and $g$ is always between the two lines. So, $F$ must cross.

This gives a rough sketch of the proof.

````{prf:example} 
logistic function iteration
(*figure of this*)
We'll iterate on a function $f(x)$: $f(x), f(f(x)), \ldots f^n(x)$
````
Now for the real proof.

````{prf:proof}
1. Let $x_0\in X$ and $x_1 = F(x_0),$ $x_2 = F(x_1)$, $\ldots x_n = F^n(x_0)$

(*Figure of the ratio of the distances between $x_0, x_1$ and $x_1, x_2$*)

2. Notice that $|x_2 - x_1| \leq K|x_1 - x_0|$

3. Continue iterating, and find

\begin{gather*}
	|x_{n+1} - x_n| \leq k^n|x_1 - x_0|
\end{gather*}

4. So, 

\begin{gather*}
	\sum_{i=1}^\infty |x_{i+1} - x_i| \leq \sum_{i=1}^\infty k^i|x_1 - x_0|\\
	= |x_1 - x_0| \sum_{i=1}^\infty k^i
\end{gather*}

Hence,

\begin{gather*}
  \sum_{i=N}^\infty |x_{i+1} - x_i| \leq |x_1 - x_0| \sum_{i=N}^\infty k^i
\end{gather*}

Therefore $\{x_i\}$ is cauchy

5. Let $x^*$ be the limit of $\{x_i\}$

6. Notice that
\begin{gather*}
	\lim_{i\to\infty}F(x_i) \to F(\lim_{i\to\infty}x_i) = x^*
\end{gather*}

and $F(x_i) = x_{i+1}$, so
\begin{gather*}
	\lim_{i\to\infty}x_{i+1} = x^* = F(x^*).\ \square
\end{gather*}
````

We further have that this point is unique if $F$ is Lipshitz (which we'd assumed).

There are a bunch more fixed point theorems that are interesting and useful. Zeidler wrote some books on the topic that are very good.

<!-- Below denotes the location of a footnote -->

