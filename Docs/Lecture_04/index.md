# Lecture 4: Derivatives are Local Approximations

## Question: What is a derivative?

The definition we like best is as follows:

Let $X, Y$ be normed linear spaces and let $f: X \to Y$. We say that $f$ is differentiable at a point $x \in X$ if there exists a linear operator $A$ such that $f(x + h) - f(x) = Ah + \mathcal{o}(h)$.

The above says that if we perturb the function by moving our input a small bit $h$ from our point $x$ then that change in the function is well modeled by some linear operator $A$, in the sense that the little bit that is left over to make our linear model exact goes to 0 faster than $h$ does.

Part of the reason the above definition is interesting is that it works for most mappings that you would care about. In physics we very often run into Euler-Lagrange equations which are really just the above in disguise.

It is true however that finding the linear operator $A$ in the above can be quite a mission, especially when we want to start doing things weakly, in which case our derivative may be a measure.

```{prf:example}
:label: Linear Maps

Let $f: \mathbb{R} \to \mathbb{R}$ be linear map from $\mathbb{R} \to \mathbb{R}$ which is nothing but multiplication by a scalar. Then we have $Ah = ah$ for some real $a$. 

```{figure} Figures/differentiable_curve.png
---
height: 350px
name: diffable_curve
---
This is a caption of an example figure
```

Then:

$f(x + h) - f(x) = ah + \mathcal{o}(h)$

$\frac{f(x+h)-f(x)}{h} = a + \frac{\mathcal{o}(h)}{h}$

Note that $\lim_{h \to 0} \left(a + \frac{\mathcal{o}(h)}{h}\right) = a$.

Hence this definition recovers our normal idea of a derivative.
``` 

```{margin}

In this example, we may also use gradient descent by examining the gradient and going in the opposite direction.
```

````{prf:example}

Let $f: \Omega \subset \mathbb{R}^2 \to \mathbb{R}$.

```{figure} Figures/3d_plot.png
---
height: 350px
name: 3d_plot
---
This is a caption of an example figure
```

Suppose we wish to minimize the energy $F$ of $f$ over all admissible $x \in \Omega$ with a restriction applied to the boundary:

$\min_f F(f) = \min_f \int_\Omega \nabla f \cdot \nabla f dx$ subject to $f|_{\partial \Omega} = g.$

Notice that $F: C^2(\Omega ; \mathbb{R}) \to \mathbb{R}$. It eats a twice continuously differentiable function and spits out a number.

We can find the minimum of the above functional the same way we would in calc 1; take a derivative and set it equal to zero.

Suppose we do that;

Notice that if we have some candidate $f$ solution. i.e. $f|_{\partial \Omega} = g$. Notice that if we take any other candidate and subtract it we will get 0 on the boundary.

Let $h = \alpha \hat{\omega}$ be a scalar multiple of some unit vector; we will vary along $h$ and look at the changes in $f$. According to our formulation:

$\int_\Omega \nabla(f + h) \cdot \nabla(f + h)dx - \int_\Omega \nabla f \cdot f dx = Ah + \mathcal{o}(h)$.

If we carry out some of the operations on the left things will simplify.

$\int \nabla f \cdot \nabla f + 2 \int \nabla f \cdot \nabla h + \int \nabla h \cdot \nabla h - \int \nabla f \cdot \nabla f = Ah + \mathcal{o}(h)$

$\int \nabla \cdot \nabla h + \int \nabla h \cdot \nabla h = Ah + \mathcal{o}(h)$

$\alpha \int \nabla f \cdot \nabla \omega + \alpha^2 \int \nabla \omega \cdot \nabla \omega = Ah + \mathcal{o}(h)$.

If we use integration by parts we get:

$-2\alpha \int_\Omega \left( \nabla \cdot \nabla f \right) \omega dx + \alpha^2 \int_\Omega \nabla w \cdot \nabla w dx = Ah + \mathcal{o}(h)$

$-2 \int_\Omega \delta f h dx + |h|^2 \int \nabla \omega \cdot \omega dx = Ah + \mathcal{o}(h)$.

Notice the first term is linear in $h$ and our second term is in fact $\mathcal{o}(h)$.
````

## Characterizing Derivatives

The characterization of a function being differentiable at a point is as follows:


```{figure} Figures/cone_property.png
---
height: 450px
name: cone_property
---
Blue Diffable function, tangent line in yellow, tangent cone in green
```

If you hand me a $\theta$ and I draw the cone with angle $\theta$ centered at that point and on that tangent line, then you can find an interval on which the curve is inside the cone, and you can do so no matter how small you make $\theta$. We call this the Cone Property and it works in any dimension.

The Cone Property proves to be extremely useful.

````{prf:example}
Suppose we have a function that is differentiable and we know its derivative is nonzero at a point. Since we know the function has nonzero slope at our point we can draw our tangent cone so that it intersects a horizontal line centered on our point in only one spot. We know that we can find an interval for which the function is entirely contained within said cone; hence the function cannot return to its previous value within some minimum distance.

```{figure} Figures/cone_example.png
---
height: 450px
name: cone_example
---
Blue Diffable function, tangent line in yellow, tangent cone in green, const line in red
```

So, know that over a bounded interval our function must only intersect the line a finite number of times (so long as each point has a nonzero derivative).
````



## Tangent Cones to Sets

We may generalize the notion of a tangent cone to a set $F \subset \mathbb{R}^n$. We will construct the tangent cone to $F$ at some point $P$.

First, we shift $F$ by $p$ to the origin for ease of calculation. We then project the intersection of $F$ with a ball of radius $\varepsilon$ onto the sphere of radius $\varepsilon$. Next, we take the closure of the resulting set since there is no guarantee it is necessarily closed. Then the tangent cone of $F$ at the point $p$ is the intersection of this set with any sequence of $\{\varepsilon_i\}_{i=1}^\infty$ going to zero.

````{prf:example}
Consider the following set in which we have a surface with a series of balls converging to a point on the surface. 

```{figure} Figures/cone_general_ex.png
---
height: 450px
name: cone_example
---
Set in red consists of balls and a surface, balls converge to the green point on surface
```

We may be tempted in our thinking to simply consider the tangent plane to our surface at the green point; however this is insufficient as it does not account for the balls converging onto that point.

Rather in this example our tangent cone will be both the tangent plane to the surface at the green point plus an orthogonal vector to the plane at that point.
````

## A Little More About Derivatives

Derivatives are all about local approximations; there are many different kinds of derivatives out there. We may even have the derivative of one measure with respect to another in which case we are looking at ratios;

Suppose we have two measures $\mu$ and $\nu$. Then when can we write $\mu(E) = \int_E \alpha(x)d\nu$, the measure of some set $E$ with respect to $\mu$ as the integral over $E$ of some function $\alpha(x)$ with respect to $\nu$. Turns out that we can do this whenever $\nu(U) = 0 \implies \mu (U) = 0$ shorthand we say this condition is equivalent to $\mu$ being absolutely continuous with respect to $\nu$ as a measure.

Question: What is $\alpha$?

$\alpha(x) = \lim_{r \to 0} \frac{\mu(B(x,r))}{\nu(B(x,r))}$

Where $B(x,r)$ is the ball centered at $x$ with radius $r$. This is called the Radon-Nikodym derivative.
