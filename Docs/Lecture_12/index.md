# Lecture 12: Forms, Currents, and Minimal Surfaces: A Bird's Eye View

## Prehistory
Plateau, a 19th century physicist from Belgium, studied soap films and formulated the problem of his namesake: given some boundary $\gamma$, is there a surface $E$ such that $\partial E = \gamma$ and $\mu(E)$ (mass of $E$) is minimal. 

Proving this result is a pretty big mission and in 1960 there were 3 solutions: 1 by Reifenberg, another by De Giorgi, and a 3rd by Federer and Fleming using currents. 

De Giorgi used sets of finite perimeter. To conceive of these, consider like lumps with some volume on which a curve is drawn which you fix and allow yourself to move the surface to try to make the surface area smaller, then as long as you're in codimension 1, then the surfaces you're looking at have boundary. Now imagine a ball with rays coming out from all the rational points. 

(*picture of a hairy ball*)

So the topological boundary could be all of space aside from inside the ball. Sets of finite perimeter take the reduced boundary. So, in this case, we cut out the rays. This is done by cutting out the portions of measure zero. This reduced boundary is what De Giorgi ended up minimizing.

The benefit is this allows you to minimize over very hairy objects. 

Federer and Fleming introduced currents into the picture. Before we can get to currents, we need to build up to forms starting with $k$-vectors and $k$-covectors.

## $k$-vectors and $k$-covectors
When thinking of a 2-vector (a pair of vectors, more or less), we can think of their exterior product as the oriented parallelograms that they generate.
\begin{gather*}
	w \wedge v = - v\wedge w
\end{gather*}
Suppose $w = a_1e_1 + a_2e_2 + a_3e_3$, likewise for $v$ with $b_i$. Then $w\wedge v$ is a 2-vector.
\begin{gather*}
	w\wedge v = (a_1b_2 - a_2 b_1) e_1\wedge e_2 + (a_3b_1 - a_1b_3) e_3\wedge e_1 + (a_3b_2 - b_3a_2) e_3\wedge e_2
\end{gather*}


The space of $2$-vectors forms a vector space with basis, in this case, $e_1\wedge e_2$, $e_2\wedge e_3$, $e_3\wedge e_1$.

We can also do this with co-vectors (the dual of the vectors): $dx_1, dx_2, dx_3$. Then the basis of the vector space would be
\begin{gather*}
	dx_1\wedge dx_2,dx_2\wedge dx_3,dx_3\wedge dx_1,
\end{gather*}
out of which we can now build $k$-covectors.

## Forms
A form is a field of $k$-covectors.

Now imagine a manifold $M$ on which every point has an associated unit 2-vector, call this field $\xi(x)$.

(*Figure of such a thing*)

We call this an oriented manifold. Now if we have a 2-from $\omega$ in space (say, $\mathbb{R}^n$), then it makes sense to take
\begin{gather*}
	\int_M \omega(\xi(x)) \, d\mathscr{H}^2(x).
\end{gather*}
Now, we use this to motivate currents.

## Currents
Notice that the integral above defines a dual vector to $\omega$, since it allows us to convert $\omega$ into a number. It's linear and bounded as well, since $M$ is compact. So, we define the currents as the dual to the space of forms.  

More precisely, a $k$-current is any dual vector to the space of $k$-forms. (Need to specify what space we're in, what's the field we're in, etc.)

A tool we'll need later on is the mass $M(T)$ of a $k$-current $T$:
\begin{gather*}
	M(T) = \sup_{|\omega|=1} T(\omega)
\end{gather*}
This turns out to be
\begin{gather*}
	M(T) = \int_{\textrm{support}(T)} |\xi|\, d\mathscr{H}^k(x)
\end{gather*}
(*Flaming sawblade*)

```{margin}
How does this relate to Kirchov's Law and Coulumb's Law?
```

Typically, we restrict ourselves to *integral currents*. 

### Integral Currents
Consider $(\xi, R, \alpha)$ where $\xi$ is the orienting $k$-vector field, $R$ is a rectifiable set and $\alpha$ is the integral multiplicity (of what?). Then we have integral currents that look like
\begin{gather*}
	T(\omega) = \int_R \alpha \omega(\xi (x))\, d\mathscr{H}^k(x)
\end{gather*}
and we require
\begin{gather*}
	M(T), M(\partial T) < \infty.
\end{gather*}
Now we need to define the boundary of $T$. We have that 
\begin{gather*}
	\partial T (\omega) \equiv T(d\omega),
\end{gather*}
which is motivated by Stoke's Theorem:
\begin{gather*}
	\int_{\partial E}\omega = \int_E d\omega.
\end{gather*}

Expanding it from our definition of $T(\omega)$:
\begin{gather*}
	T(d\omega) = \int_R \alpha \,d\omega(\xi (x))\, d\mathscr{H}^k(x) = \int_{\partial R} \alpha \omega(\hat{\xi}(x))\, d\mathscr{H}^k(x)
\end{gather*}
where $\hat{\xi}$ is generated by our orientation along the boundary of $R$.

Now, why did Federer and Fleming use currents? They could prove the following:
```{prf:theorem}
Suppose $\mathscr{T}\equiv \textrm{ all integral currents supported in } B(0,R)\textrm{ such that } M(T), M(\partial T) < C.$ Then, there exists a finite $\epsilon$-net that well approximates every $T\in\mathscr{T}$ for every $\epsilon > 0$. 
```

Chapter 5 of Morgan covers this in more detail, but the idea is that there's a polygonal curve that well approximates every compactly supported integral current.

## Flat Norm
The flat norm is defined by
\begin{gather*}
	F_\lambda(T) \equiv \inf_{S} \{M(T - \partial S) + \lambda M(S)\}
\end{gather*}
where $T$ is a $k$-current, $S$ is a $k+1$-current. 
We'll use this to get distances between currents.

Example:

(*Kiwi bird thing*)

We're partitioning between parts that have small area, and the remaining part's length. 
The green guy as max total mean curvature wrt $\lambda$.

We can get the distance between currents $T_i$ by 
\begin{gather*}
	F_\lambda(T_1 - T_2)
\end{gather*}
(figure of ufo: *$T_1$ in red, $T_2$ in blue, $\partial S$ in green*)

This distance was then used by Federer and Fleming to get convergence to minimal hypersurfaces.

## Regularity
If you look at chapter 8 in Morgan, we can consider regularity results. Regularity theory tells us things about when irregular things behave regularly.




