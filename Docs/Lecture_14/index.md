
# Lecture 14: Smooth, Compact Sets: Distance Functions, Sets With (and Without) Positive Reach, Curvature Measures, and Tube Formulas

## The idea
If we take a set $E$ 
and ask how it's volume grows as we take an $\varepsilon$ neighborhood of $E$. 

```{figure} Figures/square-tube.PNG
---
class: with-border
---
```

We find that the extra volume of this extra volume is 

\begin{gather*}
	E_\varepsilon = 4\varepsilon L + \pi \epsilon^2.
\end{gather*}

Generally speaking, for these formulas to work, the set needs to have positive reach.

## Reach

There are many equivalent definitions of positive reach, but consider the following.

Take a ball of radius $\varepsilon$ and roll it over the surface of the set. If the ball is small enough, it will touch the set at exactly one point on the boundary as we roll it around. On the other hand, if the ball is too large, the ball will hit multiple points wherever the curvature of the ball is smaller than the curvature of the set.

```{figure} Figures/peanut.PNG
---
class: with-border
---

The larger ball will clearly touch in multiple places where the peanut set dips inward.
```

The reach is the critical radius where we transition from the ball only ever touching one point to touching multiple for some regions.

An equivalent definition is to take all of the normals to our set
then follow these rays out until they touch another normal, and record the distance traveled along the ray for every ray. Then, the reach is just the minimum distance traveled along a ray before colliding with another ray.

```{figure} Figures/normals.PNG
---
class: with-border
---

The normals colide within the divit showing where the reach is limited.
```

```{margin}
Convex sets have infinite positive reach.
```

The last equivelent definition we'll discuss is to take all of the points which have a unique closest point in the set and find the smallest distance to the set among them.

## Tubes
Steiner and Minkowski investigated how the volume of sets change when we take the epsilon neighborhood.


How does the length of the boundary change if we push out a piece of the surface? 
Suppose, at some point along the boundary we have a circle that matches the boundary up to second order (such a circle is called an osculating circle) with radius $r$. Then the length of a region around that point is $L = \theta r$. If we expand by $\epsilon$, then the length becomes $L_\epsilon = \theta (r + \epsilon)$. 

```{figure} Figures/osculating-pos.PNG
---
class: with-border
---
```

Then the change in length of a segment is

\begin{gather*}
	\Delta L = \frac{\theta(r + \epsilon)}{\theta r} = 1 + \epsilon \kappa
\end{gather*}

where $\kappa$ is the curvature.

This example uses positive curvature, but what about negative curvature?

```{figure} Figures/osculating-neg.PNG
---
class: with-border
---
```

We have the same situation, but instead of growing the circle, we shrink. So, $L=\theta r$ as before, but $L_\epsilon = \theta(r - \epsilon)$. Then its easy to see that $\Delta L$ remains the same.

At this point we have everything we need to construct formulas for the change in area, the rest is algebra and the application of measures.

## Change in Area
```{margin}
To construct $\Omega + B_\epsilon$ we add to $\Omega$ every point that's within $\epsilon$ of each point in $\Omega$. This is then the $\epsilon$-neighborhood of $\Omega$.
```
Recall that we can always find an orthogonal frame on a surface that gives the principle curvature.

```{figure} Figures/principle-curves.PNG
---
class: with-border
---
```

Keeping this in mind, consider a 2d surface $\partial \Omega$ and measuring it's length after pushing it out by $\epsilon$. Then we have a new surface $\partial(\Omega + B_\epsilon)$ with

\begin{gather*}
	\mathscr{H}^2(\partial(\Omega + B_\epsilon)) &=& \int_{\partial\Omega} (1 + \epsilon\kappa_1)(1 + \epsilon\kappa_2)\, d\mathscr{H}^2 \\
	&=& \int_{\partial\Omega} 1 \, d\mathscr{H}^2 + \epsilon\int_{\partial\Omega} \underbrace{(\kappa_1 + \kappa_2)}_{\textrm{Mean Curvature}}\, d\mathscr{H}^2 + \epsilon^2\int_{\partial\Omega} \underbrace{(\kappa_1\kappa_2)}_{\textrm{Gaussian Curvature}} \, d\mathscr{H}^2 \\
\end{gather*}

where $\kappa_1, \kappa_2$ are the principle curvatures of $\partial\Omega$. Notice the first line is just taking the length times the width in the direction of the principle curvatures, and adding all of those little volumes up. This formula is our first *Tube Formula*.

Notice that in order for this to work, $\epsilon$ needs to be within the reach. If it's not, we end up losing some length after the stretching.

## Change in volume
A similar formula can be constructed for the volume of $\Omega + B_\epsilon$ by exploiting the coarea formula:

\begin{gather*}
	\mathscr{H}^3(\Omega + B_r) &=& \mathscr{H}^3(\Omega) + \int_{\epsilon=0}^{\epsilon=r} \mathscr{H}^2(\partial(\Omega + B_\epsilon))\,d\epsilon\\
	&=& \mathscr{H^3}(\Omega) + r\int_{\partial\Omega} 1\, d\mathscr{H}^2 + \frac{r^2}{2}\int_{\partial\Omega}(\kappa_1 + \kappa_2)\, d\mathscr{H}^2 + \frac{r^3}{3}\int_{\partial\Omega}\kappa_1\kappa_2\, d\mathscr{H}^2\\
	&=& a_0 + a_1r + a_2r^2 + a_3r^3
\end{gather*}

where the $a_i$ are integrals of various geometric measures. Again, $r$ must be within the reach of $\Omega$.

```{sidebar}
To justify this second step, notice that the epsilon surfaces in the $d\epsilon$ integral are generated by the distance function we're using. Then, when we look for the points within $\epsilon$ of $\Omega$ we're asking for the level sets of the distance function to $\Omega$, and the derivative of this function is 1 almost everywhere. So, $J$ in the coarea formula is $1$. The rest is just integration and exploiting our 2D result above.
```

This new formula is the Steiner-Minkowski tube formula!

## Generalizing
```{margin}
What breaks if we try to use a non-smooth surface that is concave?
```
What's wild is this works even for non-smooth convex surfaces. That means our $\kappa_i$ can be infinite. Hence, they must be redefined as measures.

Consider the solid square from earlier. After expanding by $\epsilon$ we get a square with rounded corners.

```{figure} Figures/square-tube.PNG
---
class: with-border
---
```

What part of the original square generates these portions of a circle? The flat portions of our rounded  square are each generated by exactly one point of the original square (those points are just pushed away by $\epsilon$), while the corners of the square generate entire arcs! 

Now, what can we integrate across the surface to ensure we properly generate the extra volumes that appear at the corners? If we ignore the corners for a moment and use our original 3D formula, $a_1, a_2,$ and $a_3$ are all zero, since the length only changes at the corners. So, we need to be able to detect our corners with a Dirac-delta type tool. 

Constructing as usual, we have

\begin{gather*}
	\int_{\gamma}(1 + \epsilon\kappa)\, d\ell &=& \textrm{length of } \partial(S + B_\epsilon)\\
	&=& \int_\gamma 1 \, d\ell + \epsilon\int_\gamma\kappa\, d\ell
\end{gather*}

where $S$ is the square and $\gamma$ is some path around $\partial S$. Now, we can view $\kappa d\ell$ as a new measure, call it $d\hat{\kappa}$, and we want it to be able to generate our extra chunks of length along the corners. So, we just take

\begin{gather*}
	\hat{\kappa} = \frac{\pi}{2}\sum_{i=1}^4\delta(\ell - p_i)
\end{gather*}

where the $p_i$ are our corner points.

For other shapes, all that needs to change is how much of a circle's volume is swept out by the sharp points. I.e. the solid angle of the appropriate ball.

```{prf:example}
What's the tube formula for a cube?
```

Federer first pursued this idea by noticing that after the expansion we get finite curvature everywhere. Then paying attention to how the curvatures change as we shrink back down to the original shape, we see that the limiting case is still well behaved.

