<!-- (Point For Week 5: tbd) -->

# Lecture 5: Outer Measures, Hausdorff Measures, Covering Theorems and Fractals

<!--  (Temporary title image; src: drawn by Curtis) -->

![](Figures/frustrate.png)

```{figure} Figures/example_figure.png
---
height: 350px
name: ex_fig
---
This is a caption of an example figure
```

:::{sidebar} Example Topic

This is an example of a sidebar.
:::

```{margin}
This is a margin note.
```

<!-- (Below denotes the location of a footnote) -->

[^1]

---

## Outer Measures

We return to the problem of assigning volumes to sets by measuring them. The go-to class of tools for geometric analysts are outer measures.

````{prf:definition}
:label: out_measure

Let $X$ be a set and $\mathcal{P}(X)$ denote the power set of $X$. A **outer measure** $\mu$ on $X$ is a function:

\begin{gather*}
\mu: \mathcal{P}(X) \to \bar{\mathbb{R}}
\end{gather*}

Satisfying the following two conditions:

1. The empty set has measure 0, $\mu(\emptyset) = 0$
2. The measure of a set is less than or equal to the measure of its covers, $A \subset \bigcup_{k=1}^\infty A_k \implies \mu(A) \leq \sum_{k=1}^\infty \mu(A_k)$
````

```{margin}
The power set of a set $X$ is the collection of all subsets of $X$.
```

From now on, we will use the word "measure" and "outer measure" interchangeably. We now define our measurable sets:

````{prf:definition}
:label: measureable

Let $\mu$ be a (outer) measure on $X$. Then a set $A \subseteq X$ is $\mu$-measureable if for all $B \subseteq X$:

\begin{gather*}
\mu(B) = \mu(B \cap A) + \mu(B - A)
\end{gather*}
````

```{margin}
Show this definition of measurable sets forms a $\sigma$-algebra.
```

We will restrict our attention to two particularly useful measures that we will use throughout the rest of the book: the Lebesgue measure commonly denoted $\mathcal{L}$ and its generalization the $k$-dimensional Hausdorrf measure denoted $\mathcal{H}^k$.

# Lebesgue Measure

You may be wondering why outer measures have the name they do - they called such because they involve approximating the volume of a set by covering it with other (easier to measure) sets from the outside and measuring that collection instead.

For example, suppose we have a set $E$ \subseteq $\mathbb{R}^2$ that we would like to measure. If we would like to employ the Lebesgue measure to find $\mathcal{L}(E)$ then we start by covering $E$ with rectangles.

:::{sidebar} Covers
A **cover** of a set $E$ is a collection of sets $\mathcal{A}_i$ whose union includes $E$; $E \subseteq \bigcup_i \mathcal{A}_i$
:::

```{figure} Figures/lebesgue_measure.png
---
height: 450px
name: cone_example
---
A set covered with rectangles to illustrate lebesgue measure
```

If we add up the volumes of all our rectangles we will find an approximation for the volume of $E$. Our approximation however may be very far off; there is nothing stopping us from simply tiling the entire plane with rectangles as that would still constitute a cover. What we do then is consider all possible coverings of rectangles and their associated sums and take the infimum. This is what we call the Lebesgue measure of $E$, $\mathcal{L}(E)$. In one dimension our squares are simple intervals; in $3+$ dimensions we use cubes or hypercubes.

The Lebesgue measure in 1-d, 2-d, and 3-d coincides with our normal intuitions of length, area, and volume respectively.

## Hausdorff Measure

The idea of Hausdorff measure is very similar to that of Lebesgue measure but is more general.

Let us again take some set $E$, this time we will use a curve as a example.

We will cover our set $E$ with some other sets; these are not necessarily any particular geometric shape like balls or rectangles.

We will then measure each set with a gauge function; if we enumerate our sets $\{E_i\}_{i=1}^\infty$ then our chosen gauge function $\gamma(E_i) = \text{diam}(E_i)$ tells us the diameter of our covering sets.

```{margin}
The diameter of any set $A$ in a metric space is defined as $\text{diam}(A) = \sup \{ |x-y| : x,y \in A \}$
```

Then if we add up the diameters of all our sets $\sum_{i=1}^\infty \gamma(E_i)$ then we have a rough approximation for the length of the curve $E$. 

We could improve this approximation if we made our sets not intersect much and arranged them correctly. To do this, we would like to shrink the diameter of our sets to make our cover close in on the curve.

Generally,

$$\mathcal{H}_\delta^k(E) = \inf\limits_{\substack{\text{diam}(E_i) \leq \delta \\ c \in C_\delta}} \alpha(k) \sum_{i} \left(\frac{\text{diam}(E_i)}{2}\right)^k$$

Where $\alpha(k)$ is the volume of the $k-$dim unit ball, $c$ is an element of a cover $C_\delta$ whose elements $E_i$ have a diameter no more than some specified $\delta$. Then the $k$-dim Hausdorff measure of $E$ is given by:

$\mathcal{H}^k(E) = \lim_{\delta \to 0} \mathcal{H}_\delta^k(E)$

We choose $\delta$ to select the permissible covers for our set; a cover is permissible if every set in the cover has a diameter of $\delta$ or less.

We then think of all such possible covers $C_\delta$ and do the indicated sum. Then we take the greatest lower bound on the pile of numbers we get and that will be what we call $\mathcal{H}_\delta^k(E)$. We then take the limit as $\delta \to 0$.

Note that as we take this limit we are simply restricting our covers to a subset of what we had originally; thus our infimum must either stay the same or grow larger monotonically. Hence, if we allow values of $\infty$ we know the limit exists.

```{figure} Figures/hausdorff_example.png
---
height: 450px
name: hausdorff_example
---
Showing how we can measure a set using covers / Hausdorff measure.
```

## Vitali's Covering Theorem

```{prf:theorem}  Vitali's Covering Theorem

Let $E \subseteq \mathbb{R}^d$. Given any cover $\mathcal{A}$ of $E$ by closed balls with finite radius, we can restrict $\mathcal{A}$ to a countable subcollection of pairwise-disjoint balls that, when dialated to 5 times their radius, still cover $E$.

We will outline the proof.

Let $\mathcal{A}$ be a collection of balls in $\mathbb{R}^n$, each with a radius less than some number $C$. Let $r(a)$ denote the radius of any ball $a \in \mathcal{A}$.

1. First, we split $\mathcal{A}$ amongst bags $\{\varepsilon_k \}_{k=1}^\infty$ according to the following rule describing the $k$th bag:

\begin{gather*}
\varepsilon_k = \left\{ a \in \mathcal{A} : \frac{C}{2^k} < r(a) \leq \frac{C}{2^{k-1}} \right\}
\end{gather*}


2. Use Zorn's lemma to construct a countable maximal collection $F_1$ of pairwise disjoint balls in $\varepsilon_1$; note that if we expand the radius of each ball $u$ by 5 (denoted 5f for short) then we have:

\begin{gather*}
\bigcup_{u \in \varepsilon_1} u \subset \bigcup_{u \in F_1} 5u
\end{gather*}

3. We generalize the construction of $F_i$ from $\varepsilon_i$.

First. remove any balls that intersect any ball in the bags $\bigcup_{i=1}^{i-1} F_i$ before it.

Second, use Zorn's lemma again to find a countable maximal collection of pairwise disjoint collection of what is left.

With the above, we have constructed a countable collection $F := \bigcup_{i=1}^\infty F_i$ of pairwise disjoint balls from $\mathcal{A}$ such that, when we dialate each ball by a factor of 5 we cover $\bigcup \mathcal{A}$.
```

We will now see an application of Vitali's Covering Theorem.

## Sard's Theorem on the Real Number Line

Let $f: \mathbb{R} \to \mathbb{R}$ be a smooth function.

(Graphic: Smooth Function)

A point $c$ is a **regular point** of $f$ if $f^{-1}(c)$ contains only points at which the derivative of $f$ is full rank. Recall that the derivative is a linear operator that, for smooth functions, mimicks the function in some neighborhood. The only way the derivative in this case is not "full rank" is if $f'(x) = 0$.

```{prf:theorem}  Sard's Theorem (Real Number Line)

Let $f: \mathbb{R} \to \mathbb{R}$ be smooth. Then the image of all points $x$ where $f'(x) = 0$ has measure 0, i.e.:

\begin{gather*}
H^1 (f(\{x : f'(x) = 0\})) = 0
\end{gather*}

We will outline a proof.

Suppose we are working with some compact interval $K$. Choose some $\varepsilon > 0$ and define $E := \{ x | f'(x) = 0 \} \cap K$

Note that generally if you can prove a property of a space on compact subspaces then it's true for your entire space; if your function is defined on more than a compact set then you break it up into compact sets and prove it on each set.

Define $B_x :=$ to be the ball centered at $x$ with small enough radius (this is just an open interval since we're in $\mathbb{R}$) $r_x$ so that $y \in B_x \implies $ graph of $f$ at $y$ is in the cone with tangent $\varepsilon$ and the ball does not leave $K + B(0,\varepsilon)$ (Note that this is a Minkowski sum; you take $K$ and add to it all points within $\varepsilon$ - intuitively we've made it a little fatter and now it's an open set)

```{figure} Figures/cone_with_numline.png
---
height: 350px
name: ex_fig
---
Sample caption
```

Figure note: Epsilon controls the angle of the cone, we can choose epsilon small enough so that any graph is inside said cone (the cone has a tangent of epsilon so going over 1 goes up epsilon) (we can do this because $f$ is diffable)

Define $\hat{B_x} := \frac{1}{5} B_x$ (note that this product is like Minkowski multiplication; multiply every value in the set by $\frac{1}{5}$). Clearly the set covers.

If we apply Vitali's theorem, we see that $\hat{B_{x_i}$ is disjoint and its dialation by 5 is a cover. And we have that $E \subseteq \bigcup_{i=1}^\infty 5 \hat{B_{x_i}}$. We know that inside those balls the graph lives inside the cones.

The above implies that in particular that we have covered the set with disjoint balls. Because they are disjoint we know that the measure $\mathcal{L}^1(\bigcup \hat{B_{x_i}}) = \sum \mathcal{L}^1( \hat{B_{x_i}}) \leq \mathcal{L}^1(K + b(0,\varepsilon)) = \mathcal{L}^1(K) + 2\varepsilon$.

The $2\varepsilon$ comes from the fact that $K$ is an interval.

$\mathcal{L}^1(f(E)) \leq \sum \mathcal{L}^1(f(5\hat{B_{x_i}}))$ since $f(E)$ is contained. And by the cone argument  $\mathcal{L}^1(f(E)) \leq \sum \varepsilon 5 \mathcal{L}^1(\hat{B_{x_i}}) = \varepsilon 5 \sum_{i=1}^\infty \mathcal{L}^1(\hat{B}_{x_i}) \leq \varepsilon 5 (\mathcal{L}^1(k) + 2 \varepsilon)$ and $\varepsilon$ was abirtrary; since this is true for all $\varepsilon > 0$ this implies that $\mathcal{L}^1(f(E)) = 0$.

The key to the above is that we know the derivative is zero here so we can make our cones have a tangent of zero and can be made arbitrarily skinny.

```{prf:corollary}

If you take an open set $U$ and pick any $\delta$ then you can pick a disjoint collection of closed balls in $U$ ($U$ itself may be wild) such that the union of those balls eat up all the measure inside $U$ - it doesn't necessary eat up every point, but what is left has measure zero. (This essentially says that if I can control something over all but a set of measure zero then I've controlled it generally)

```

We would like to contrast the above with another covering theorem.

## Besicovitch's Covering Theorem

Besicovitch's overing theorem is an homolog of Vitali's covering theorem that does not require us to enlarge the balls in our cover.

```{prf:theorem}  Besicovitch's Covering Theorem

Let $F$ be a collection of closed balls of bounded diameter in $\mathbb{R}^n$ and denote the centers of said balls by $A$. Then there exist countable collections of disjoint balls $G_1, G_2, ..., G_m \subseteq F$ from $F$ that together cover the centers:

\begin{gather*}
A \subseteq \bigcup_{i=1}^m \bigcup_{B \in G_i} B
\end{gather*}

Noting that the constant $m$ depends only on the dimension, $n$.
```

The proof of Besicovitch's covering theorem is involved and so we do not cover it here. The major payoff for us is in the following corollary:

```{prf:corollary}
:label: my-corollary

Let $\mu$ be a Borel measure on $\mathbb{R}^n$ and $F$ some collection of closed balls with $A$ the collection of their centers. If the measure of $A$ is finite $\mu(A) < \infty$ and inf $\{ r : B(a,r) \in F \} = 0$ for every $a \in A$ then for every open set $U \subseteq \mathbb{R}^n$ there is a countable collection of disjoint balls $G \subseteq F$ from $F$ such that:

\begin{gather*}
\bigcup_{B \in G} B \subseteq U
\end{gather*}

and

\begin{gather*}
\mu\left( (A \cap U) - \bigcup_{B \in G} B \right) = 0
\end{gather*}
```

The interpretation is that if we are handed some open set $U \subseteq \mathbb{R}^n$ then we can fill it with a countable collection of disjoint balls so that what is left over has measure 0.

Book: Measure Theory and Fine Properties of Functions (Studies in Advanced Mathematics) by Evans and Gariepy

## Tangent Measures

Idea: recall approximate tangent plane, this was a special case of a tangent measure

Ingredients:

1. $r_i \to 0$
2. A point $x$ with support of our measure $\mu$.
3. $\phi$ smooth test function
4. Measure $\mu$

```{figure} Figures/support.png
---
height: 350px
name: ex_fig
---
Absolute val function; the yellow line is the weak derivative
```

Blow things up (dilate) by exactly $\frac{1}{r_i}$ and then integrate measure against $\phi$.

(Funky notation) $\mu_{r_i}$ indicates the measure blown up by the factor. If as $i \to \infty$:

\begin{gather*}
\int \phi d\mu_{r_i} \to \int \phi d\nu \forall \phi
\end{gather*}

then $\mu_{r_i} \to \nu$.

If we blow space up but keep the measure the same (we zoom in) and we integrate the measure against something in there but we don't increase the think of a 1-d piece and restrict the measure to that set (Figure) if we zoom in to that and integrate a function against it that's the same as contracting the function. If you make the function smaller it's the same as if you zoom out on the set.

So we need to renormalize things by (in this case) a factor of $r_i$. In higher dim it's $r_i^\alpha$.

Intuitively we're blowing things up and keeping a smooth function fixed and it only sees what the measure is locally.

Example:

A measure that has two different tangent vectors at a given point.


```{figure} Figures/lines.png
---
height: 350px
name: ex_fig
---
Sample caption
```

Parent line has len L, next line has len 1, zoom in, twist, zoom in, twist cut, zoom in, twist, cut, etc ratio of stuff that gets twisted is going to zero. We can pick either twist or no twist as our result.

(Graphic: top graph thing? Idk i was thinking about sets containing themselves)

## Support of a measure vs. defining set

We briefly discuss the support of a measure versus the set its defined on. For "nice" smooth objects, the support for the measure and its defining set are the same thing, but generally they are not.

The support $spt(\mu)$ of a Borel measure $\mu$ on a set $X$ is the relative complement of the union of all open sets whose measure is 0 under $\mu$. It is the smallest closed set $C$ such that $\mu(X - C) = 0$ and can be thought of the space in $X$ where the measure "lives".

Example: $E = \mathbb{Q} \cap [0,1] \times [0,1]$ the rational numbers restricted to the unit square.

Note that $E$ is countable. Define $K := \bigcup_{i=1}^\infty \partial B(q_i, \frac{1}{2\pi} \frac{\varepsilon}{2^I}) \cap [0,1]^2$. (Circles) then the length: $H^1(K) = \varepsilon$ is $\varepsilon$.

The size of the defining set is tiny. However the support of $H^1$ restricted to $K$ contains the entire unit square $[0,1]^2$. This shows that in general, the support of a measure is not the same as the set it is defined on.

## Measure theoretic whatevers (limits..continuity..limsup..liminf..etc..)

Often we speak of something (limits, continuity, etc...) being measure theoretically true at some point $x \in \mathbb{R}^n$. What we mean when we say this is that the statement is true if we ignore some set $B$ with density $0$ at $x$.

````{prf:definition}
:label: density

Let $B \subseteq \mathbb{R}^n$. For $1 \leq m \leq n$ and some $x \in \mathbb{R}^n$ we define the $m$-dimensional density of $B$ at $x$, denoted $\Theta^m(B,x)$ by:

\begin{gather*}
\Theta^m(B,x) &= \lim_{r \to 0} \frac{\mathcal{H}^m(A \cap B^n(a,r))}{\alpha_m r^m}
\end{gather*}

Where $\alpha_m$ is the $m$-dimensional measure of the closed unit ball in $\mathbb{R}^m$. Note that if we wished we could extend this definition to be with respect to some other measure $\mu$. 
````

```{figure} Figures/point.png
---
height: 350px
name: ex_fig
---
Sample caption
```

In the figure, $B$ has density 0 at $x$ because $\frac{B(x,r) \cap E}{\pi r^2} \to 0$ as $r \to 0$. The fraction of the ball occupied by $E$ goes to 0 as we shrink the ball, then it has measure 0.

## Weak Deriviatves

Theme: if a function has a certain property (like it's diffabl) then something is true; but it turns out that maybe diffable functions have that property being true so we can shift the def'n and say these things we are intertested in are weakly diffable.

$\phi$ smooth and have compact support in some open set $U$ ($U$ must have the compact support of $\phi$).

```{figure} Figures/weak_1.png
---
height: 350px
name: ex_fig
---
$U$ with the compact support of $\phi$
```

```{margin}
A function has **compact support** if its support is a compact set. In $\mathbb{R}^n$ a function has compact support if and only if its support is bounded.
```

Know: $0 = \int_U (f\phi)'dx$. We only need to integrate where $\phi$ is not zero. The product rule says $= \int_U f'\phi + \int_U f\phi'$. Since it equals zero: $\int_U f'\phi dx = - \int_U f\phi' dx$. If $f$ is diffable that's true for every smooth guy. Now we do our trick:

Suppose there is a function $g$ such that this is true:

$\int_U g \phi dx = - \int_U f\phi$ for all smooth guys; then it's acting like a derivative of $f$. Then we name $g$ the weak derivative of $f$. This ignores something that happens at measure zero; inherited from the integral.

If you do this

```{figure} Figures/weak2.png
---
height: 350px
name: ex_fig
---
Absolute val function; the yellow line is the weak derivative
```

(Note: weak derivatives are linear)

In higher dimensions:

Suppose you have a function $f: \mathbb{R}^n \to \mathbb{R}$. Then $\phi = \mathbb{R}^n \to \mathbb{R}^n$ smooth. (They are Smooth Vector fields w/ compact support in $U$)

$0 = \int_U \nabla \cdot (f\phi) dx = \int_U \nabla f \cdot \phi dx + \int_U f \nabla \cdot \phi dx$ We know the first integral is $= \int_{\partial U} f \phi \cdot \vec{n}d\sigma$ by div theorem since $\phi$ is 0 on the boundary. Then:

$\int_U \nabla f \cdot \phi dx = - \int_U f \nabla \cdot \phi dx$. If we can find some vector field $\vec{g}$ s.t. for all test functions: $\int \vec{g} \cdot \phi dx = - \int_U f \nabla \cdot \phi dx$.

Aside:

Notation for PDEs/Soblev Spaces: $W^{1,2}$ means you have $1$ derivative and both $f$ and $f'$ are in $L^2$. $W^1 \implies \int_\mathbb{R} |f| dx$ and $\int_\mathbb{R} |f|dx$ both finite. If there is a 2 there then $|f|^2$ and $|f^2|$. (Weak derivatives show up a lot here)

We have thus used integration and differentiation to extend our notion of differentiation.

---

[^1]: This is a footnote as it appears at the bottom of the page.
************