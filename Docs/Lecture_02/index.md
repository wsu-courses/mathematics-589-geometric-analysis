# Lecture 2: Metric Spaces: A Concise Tour

![](Figures/wk2_title_image.png)

A **Topology** defined on a set is a minimal structure to do useful calculations within that set. However, many topological spaces are too wild to be practical. To get a handle on topological spaces, additional structure can be added by inheriting our topology from a metric.

:::{sidebar} Topology

Suppose $X$ is a non-empty set. A set $\tau$ of subsets of $X$ is called a **Topology** if:

1. $X$ and $\emptyset$ belong to $\tau$.
2. The arbitrary union of sets in $\tau$ also belong to $\tau$.
3. The intersection of any finite number of sets in $\tau$ belongs to $\tau$.

The pair $(X, \tau)$ is called a **Topological Space**. Intuitively, a topology is a list of the open sets of $X$.
:::

A **metric space** is a pair $(X,\rho)$ where $X$ is a set (consisting of elements we call points) and $\rho: X \times X \to \mathbb{R}$ is a function (called a metric, informally a ruler) that measures distance between points in $X$. This ruler must satisfy the following intuitive notions of distance to qualify as a metric:

1. (Positivity) For all $x,y \in X$: $\rho(x,y) \geq 0$ and 0 if and only if $x = y$.

2. (Symmetry) For all $x,y \in X$: $\rho(x,y) = \rho(y,x)$.

3. (Triangle Inequality) For all $x,y,z \in X: \rho(x,z) \leq \rho(x,y) + d(y,z)$.

```{prf:example}

$(\mathbb{R}^n,\rho)$ with $\rho$ defined below is the prototypical metric space:

$$
\begin{align*}
\rho(\mathbf{x},\mathbf{y}) &:= ||\mathbf{x}-\mathbf{y}||\\
&= \sqrt{\sum_{i=1}^n (x_i-y_i)^2}
\end{align*}
$$
```

:::{sidebar} Open ball
$B(e,\varepsilon)$ is read: "Open ball of radius $\varepsilon$ centered on $e$, $B(e;\varepsilon):= \{ x \in X : \rho(x,e) < \varepsilon \}$
:::
If we choose to inherit a topology from our metric, we say that a set $E \subset X$ is **open** (belongs to the topology $\tau$) if for every $e \in E$ there exists an $\varepsilon > 0$ such that $B(e;\varepsilon) \subset E$.
That is, we can find an open ball with some positive radius centered on every point in the set that lies entirely within said set. Primitively, one may think of an open set as a set without its boundary.


```{figure} Figures/wk2_open_set.png
---
height: 250px
name: open_set
---
An open ball centered on $e \in E$.
```

```{prf:proof} Open balls are open.

Let $(X,\rho)$ be a metric space and consider $E = B(x;r)$, an open ball of radius $r$ centered on any point $x \in X$. Take any $e \in E$ and consider the open ball $B(e;\varepsilon)$ where $\varepsilon = r-\rho(x,e)$ is the distance from $e$ to the edge of $E$. For any $y \in B(e;\varepsilon)$ we have by the triangle inequality:

\begin{align*}
\rho(x,y) &\leq \rho(x,e) + \rho(e,y)\\
&< \rho(x,e) + r - \rho(x,e)\\
&= r
\end{align*}

Since the distance from $x$ to $y$ is less than $r$, we know that $y \in E$. Since this holds for any $e \in E$, we know we can always find a ball of some positive radius $\varepsilon$ centered on any point in $E$ that is contained entirely within $E$.

$E$ is therefore an open set.
```

```{margin}

The compliment of a set $X \subseteq U$ denoted $X^c$ or $U \setminus X$ is the parent set $U$ with $X$ removed.
```
A set is **closed** if its compliment is open. Alternatively, we may also think of a closed set as one that contains all of its limit points (points which have infinitely many nearby neighbors in the set).


#### Examples of Metrics

The standard metric we think of in Euclidean space is called the Euclidean metric $\rho(x,y)$; $n$-dimensional Euclidean space is then $(\mathbb{R}^n, \rho(x,y))$ with:

$$
\begin{gather*}
\rho(x,y) := \sqrt{(x_1-y_1)^2 + \cdots + (x_n-y_n)^2}\\
= \sqrt{\sum_{i=1}^n (x_i-y_i)^2}
\end{gather*}
$$

We may generalize the above notion into the $p$-metric $\rho(x,y)_p$:

$$
\begin{gather*}
\rho_p(x,y) := \sqrt[p]{\sum_{i=1}^n |x_i-y_i|^p}
\end{gather*}
$$



And as a special case we may consider the $\infty$-metric $\rho_\infty(f(x),g(x))$ over integrable functions $f,g$ as:

$$
\begin{gather*}
\rho_\infty(f,g) := \lim_{p\to\infty} \left(\int_0^1 |f-g|^p dx) \right)^{\frac{1}{p}}
\end{gather*}
$$

:::{sidebar} Supremum

The **supremum** or **sup** of a set is the least upper bound of that set. For example, $\sup(\left\{ \frac{1}{2}, \frac{3}{4}, \frac{7}{8} , \frac{15}{16},...\right\})$ is equal to 1, even though 1 is not a member.
:::

The **norm** of $x$ is given by $\rho(x,0)$ and is thought of as either the distance of $x$ from the origin or the length of $x$ as a vector. For a function $f$, the $\infty-norm$ of $f$ given by $|f|_\infty := \rho_\infty(f,0) = \lim_{p\to\infty} \left(\int_0^1 |f|^p dx) \right)^{\frac{1}{p}}$ finds the supremum of $f$ ignoring sets of measure 0; hence we may refer to it as the **essential supremum**.

```{figure} Figures/wk2_true_sup.png
---
height: 450px
name: true_sup
---
A function (blue points and red line) whose essential supremum is given by the lower yellow line; the blue points form a set of measure 0 and are hence ignored.
```

## The Five C's

### Continuity

```{figure} Figures/wk2_cont_func.png
---
height: 350px
name: cont_func
---
An image of a continuous function (blue) and a discontinuous function (green)
```

A function $f: X \to Y$ is continuous if the preimages

:::{sidebar} Preimages of sets

The preimage of a set $K \subset Y$: $f^{-1}(K) := \{ x \in X : f(x) \in K \}$ is itself a set (of all those things in $X$ that come to lie in $K$) and does not necessarily require an inverse function exist.
:::

of open sets are open. In other words, if $K \subseteq Y$ is open, then $f^{-1}(K)$ is necessarily open. Note that the sets $X, Y, \emptyset$ are always themselves open.

```{note}
Some may find it strange that $X$ and $Y$ are open for continuous maps $f:X\to Y$. For example, the map $f:[0,1]\to \Bbb{R}$ defined by $f(x) = x$ is clearly continous, but $[0,1]$ is closed. So, what gives?

The interval $[0,1]$ is closed in $\Bbb{R}$ with the usual metric, but it is open in $[0,1]$. That is to say $X$ and $Y$ are open when viewed from within themselves, rather than the larger context one may be used to viewing them from. 
```

Application:

```{prf:theorem}  Intermediate Value Theorem (IVT)

Let $f: \mathbb{R} \to \mathbb{R}$ be a function that is continuous over an interval $[a,b]$. Furthermore, suppose that $f(a)$ and $f(b)$ have opposite signs, then $f$ has crossed the $x$ axis at some point $c \in [a,b]$. 
```

### Compactness

```{figure} Figures/wk2_open_cover.png
---
height: 450px
name: open_cover
---
A collection of open sets (blue) covering a set (red)
```

We can cover a set $A$ with a collection of sets $U_i$ if $A \subseteq \bigcup_i U$, in which case we call $U_i$ a **cover** (of $A$). If every set in our collection $U_i$ is an open set, then $U_i$ is an $\textbf{open cover}$.

A set $K$ is **compact** if every open cover of $K$ contains a finite subcover (a subset of $U_i$ that still covers $K$). That is, if given any open cover $U_i$ of $K$ we can always reduce our cover to a finite collection that still covers our set, then our set is compact. Compact sets are typically "nice" in the sense that they unlock the use of many powerful theorems.

#### Application: Extreme Value Theorem

```{figure} Figures/wk2_extreme_value_thrm.png
---
height: 350px
name: extreme_value_thrm
---
Maximal and miminal points of a function $f$
```

```{prf:theorem} Extreme Value Theorem

Suppose $K \neq \emptyset$ is a compact subset of a metric space $(X,\rho)$ and let $f:K \to \mathbb{R}$ be continuous. Then there exists $x_m,x_M \in X$ called the minimum and maximum points (respectively) such that $\forall x \in K:$

\begin{gather*}
f(x_m) \leq f(x) \leq f(x_M)
\end{gather*}
```

In $\mathbb{R}^n$, a set is compact if and only if it is closed and bounded - in more exotic metric spaces this is not necessarily true.

### Connectedness

A metric space $(X,\rho)$ is **disconnected** if there exists at least two sets $K_1, K_2 \neq \emptyset$ such that

$$
\begin{gather*}
X = K_1 \cup K_2 \text{ and }\\
K_1 \cap K_2 = \emptyset
\end{gather*}
$$



If there does not exist **any** such pair of sets, then the metric space is by definition **connected**.

Said metric space is **path-connected** if for all $x,y \in X$ there exists a continuous function $\gamma:[0,1] \to X$ such that $\gamma(0) = x$ and $\gamma(1) = y$. Every path-connected metric space is connected, but the converse does not hold in general.

```{margin}
Such a $\gamma$ is called a **path** from $x$ to $y$.
```

If $f$ is a continuous function and $E$ is a connected set then $f(E)$ is connected. Likewise, if $E$ is path-connected its image under a continuous $f$ is also path-connected.

### Convergence

```{figure} Figures/wk2_convergence.png
---
height: 250px
name: convergence
---
A sequence of points (yellow) converging to a limit point (red)
```
:::{sidebar} Limit point

A point $x \in X$ is a **limit point** of a set $A \subseteq X$ if every open set containing $x$ contains some other point of $A$.
:::
A **sequence** of points $\{x_i\}_{i=1}^\infty$ in a metric space $(X,\rho)$ converges to a limiting point $x^*$ if $\rho(x_i,x^*) \to 0$ as $i \to \infty$.

```{margin}
A sequence $\{x_1,x_2,...\}$ is an infinite collection of points that can be ordered and counted.
```

### Cauchy

```{figure} Figures/wk2_cauchy_1.png
---
height: 350px
name: cauchy_1
---
A sequence of points that alternates between left and right; the subsequence formed by points only on the left or right converge to their own respective limits.
```

A sequence $\{ x_i \}_{i=1}^\infty$ in a metric space is Cauchy if for every real $\varepsilon > 0$ there exists some $N_\varepsilon \in \mathbb{N}$ such that for all indicies $i,j > N_\varepsilon$

```{margin}
$N_\varepsilon$ is denoted as such as it may depend on the fixed $\varepsilon$
```

$$
\begin{gather*}
\rho(x_i,x_j) \leq \varepsilon
\end{gather*}
$$

Intuitively, all the terms of the sequence get jammed together.

A space is **complete** if every such Cauchy sequence converges to point within the space.

Let $(X,\rho)$ be a metric space and let $K \subseteq X$ be compact. Then for any sequence $\{ x_i \}_{i=1}^\infty \subset K$, there exists a subsequence $\{ x_{i_{n}} \}_{n=1}^\infty$ and an $x^* \in K$ such that $x_{i_{n}} \to x^*$ as $n \to \infty$. In other words, every sequence in a compact space has a subsequence that converges in the space.

```{figure} Figures/wk2_cauchy_2.png
---
height: 350px
name: cauchy_2
---
A Cauchy sequence of points formed by rotating noninterger multiples around a circle.
```

## Functions

### Limsup and Liminf

```{figure} Figures/wk2_limsup_liminf.png
---
height: 350px
name: limsup_liminf
---
The limsup and liminf of a sequence for various $n$.
```

Suppose we have a sequence $\{x_i\}_{i=1}^\infty$ and we examine the $n$th term $x_n$. We may think of all the terms including and beyond $n$ as a set $\{x_n, x_{n+1}, x_{n+2},...\}$ - a pile of numbers - and ask ourselves what the sup of this set is. As we advance $n$, we may continue considering the sup of what remains. This sequence of supremums forms its own sequence - one that can only decrease as we throw more and more terms away. If we take the limit as $n \to \infty$ we have arrived at the notion of the $\limsup$. If we allow values of $\pm \infty$; the limsup will always exist. Furthermore, because said sequence of supremums is only decreasing or remaining constant we classify it as monotonic, in general such sequences are comparably well-behaved. A similar notion is developed for $\liminf$ by considering the infimum of $\{x_n, x_{n+1}, \ldots}$ and taking $n\to\infty$. 

The above may also be extended to functions, in which case we have an alternative definition of continuity; $\limsup f = \liminf f$ then $f$ is continuous. Such an $f$ will also be continuous if for every sequence $x_i \to x^*$ we have $f(x_i) \to f(x^*)$.

### Preserved Properties

Let $f: X \to Y$ be a function and let $A$ be an arbitrary collection of subsets of $X$. Then we have the following:

$$
\begin{gather*}
f\left( \bigcup A \right) = \bigcup f(A) \text{ and }\\
f\left( \bigcap A \right) \subseteq \bigcap f(A)
\end{gather*}
$$



That is, the image of a union of sets is equal to the union of the images. In general we do not have equality for intersections unless $f$ is an injection.
:::{sidebar} Injection

We say a function $f:X\to Y$ is an *injection* if $f(x) = f(y)$ implies $x = y$.
:::

Let $B$ be an arbitrary collection of subsets of $Y$, then for preimages we have:

$$
\begin{gather*}
f^{-1}\left(\bigcup B \right) = \bigcup f^{-1}(B) \text{ and }\\
f^{-1}\left(\bigcap B \right) = \bigcap f^{-1}(B)
\end{gather*}
$$