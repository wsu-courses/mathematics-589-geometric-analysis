# Neutron Stars

:::{figure} https://upload.wikimedia.org/wikipedia/commons/c/c9/Chandra-crab.jpg
---
name: CrabNebula
---
A composite image of the Crab Nebula showing the X-ray (blue), and optical (red) images
superimposed.  The Crab Pulsar in the Crab Nebula is a rapidly spinning neutron star the
exhibits a puzzling phenomena called glitching.  Along with the general trend of the
rotation speed slowing down as the pulsar loses angular momentum to electromagnetic and
gravitational radiation, one sees occasional periods where the rotation rate suddenly
rapidly increases.  These glitches are an example of the type of observations we can
make on earth to try to deduce what is happening inside these remote objects.  Obtaining
this data takes an extraordinary amount of effort, from building and launching intricate
telescopes, to extended monitoring of the objects, and finally analyzing the data
extract the signals.  Then, we must try to model these signals using a combination of
the best nuclear theory with large-scale simulations to try to capture the glitching
mechanism. 
:::

Neutron stars comprise the densest form of nuclear matter in the universe.  They sit at
the cusp of stability, poised on the brink of collapsing into a spacetime singularity
called a black hole.  After massive stars run out of fuel, they explode in cataclysmic
events called supernovae, then the remaining core of the star collapses under the
attractive effects of gravity, compressing between one and two times the mass of our sun
into an sphere of about 12km radius.

I say "about" here because we do not understand the properties of neutron-rich matter
accurately enough to make definite predictions about the radius of a star with given
mass (see {numref}`NeutronStarMassRadius`).  One of the goals of our research is to use
the handful of observations available to constrain the properties of nuclear matter so
that we can predict how dense matter will behave.

:::{figure} NeutronStarMassRadius.*
---
figclass: margin-caption
width: 100%
name: NeutronStarMassRadius
---
Sample neutron star mass-radius curves from {cite:p}`Forbes:2019`.  The radius $R$ of a
neutron star of mass $M$ (expressed here in units of the sun's mass $M_{\odot} \approx
10^{24}$kg) depends on properties of neutron-rich nuclear matter, which also has
relevance for understanding the formation of elements in our universe, and finding clean
reaction chains for nuclear energy.  One goal of our research is to extract as much
information from available observations of neutron stars to constrain nuclear physics.
:::

:::{figure} https://www.ligo.caltech.edu/system/avm_image_sqls/binaries/52/page/HiResHanford_5.jpg
---
figclass: margin
---
[LIGO] Laboratory operates two detector sites, one near Hanford in eastern Washington (a 3
hour drive west from Pullman).  Laser interferometers compare light bouncing back and
forth down these two perpendicular beam tubes (each 4km long) can detect fractional
changes in length of one part in $10^{23}$ caused by gravitational waves passing through
the earth.
:::

A variety of data analysis techniques are needed to understand what his happening in
neutron stars.  Most recently, a new technique successfully "heard" the merging of two
neutron stars through the gravitational waves they emit when they collide.  The Laser Interferometer
Gravitational-Wave Observatory ([LIGO]) can hear the ripples in spacetime produced when
neutron stars merge.  This remarkable feat was recognized by the [2017 Nobel prize in
Physics](https://www.ligo.caltech.edu/page/press-release-2017-nobel-prize), and requires
many different pieces of data analysis.

First, one must find and distinguish gravitational signals from other noise, like
passing trucks, earthquakes, power, etc.  This requires matching the terabytes of data
collected each day when operational.  While this data can be processed offline,
real-time triggering is required so that other observatories can be alerted to merger
events, collecting additional electromagnetic information (light, x-rays etc.)

Once this data is collected an processed, we obtain estimates for the mass, radius, and
tidal deformability of the neutron stars that merged.  Each such piece of data therefor
requires an enormous amount of work.  Our second task is to use the handful of merger
events to optimally constrain nuclear physics.  This task lies at the opposite end of
the data-analysis spectrum: instead of trying to process copious amounts of data, we must
extract as much information as possible from a very small set of extremely expensive data.

This information provides us with a window into the behaviour of neutron-rich matter,
allowing us to better constrain our theories of nuclear interactions.  Constraining
nuclear theory is critical for fundamental science, allowing us to answer the question
about how the elements on earth formed from exploding stars.  It is also essential to
develop reliable predictive technology to explore clean pathways for nuclear energy, and
to design safe reactors.

[LIGO]: https://www.ligo.caltech.edu/

:::{admonition} Jets and Debris from Neutron Star Collision

<iframe width="560" height="315" src="https://www.youtube.com/embed/e7LcmWiclOs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This animation captures phenomena observed over the course of nine days following the
neutron star merger known as GW170817. They include gravitational waves (pale arcs); a
near-light-speed jet that produced gamma rays (magenta); expanding debris from a
"kilonova" that produced ultraviolet (violet), optical and infrared (blue-white to red)
emission; and, once the jet directed toward us expanded into our view from Earth, X-rays
(blue).

Credit: NASA's Goddard Space Flight Center/CI Lab
:::

:::{admonition} Ripples of Gravity, Flashes of Light

<iframe width="560" height="315" src="https://www.youtube.com/embed/EtIkOjq0_50"
title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;
clipboard-write; encrypted-media; gyroscope; picture-in-picture"
allowfullscreen></iframe>

On Aug. 17, 2017, the Laser Interferometer Gravitational-wave Observatory (LIGO) and Virgo [detected, for the first time](https://www.caltech.edu/about/news/ligo-and-virgo-make-first-detection-gravitational-waves-produced-colliding-neutron-stars-80082), gravitational waves from the collision of two neutron stars. The event was not only “heard” in gravitational waves but also seen in light by dozens of telescopes on the ground and in space. Learn more about what this rare astronomy event taught us in a new video from LIGO and Virgo.

Credit: LIGO-Virgo
:::
