# Density Matrices: A Convex Application

:::{margin}
By "local" we mean the parts of a system that we have access to.  I.e. the parts we can manipulate, measure etc.  In the well-known [EPR] thought experiment {cite:p}`Einstein:1935`, two subsystems are physically separated so that measurements on one subsystem cannot influence measurements on the other because of the finite speed of light.

:::
In quantum mechanics, the state of a local quantum system can be completely describe by
a [density matrix] $\mat{\rho}$ (the matrix elements of a density operator expressed in
some convenient basis), which has the following properties.

:::{prf:definition} Density Matrix
:label: density-matrix

A *density matrix* is a [positive semi-definite] [Hermitian matrix] of trace one.
:::

:::{admonition} Details
:class: dropdown

1. **Hermitian:** the matrix is equal to the transpose of the complex conjugate 

   \begin{gather*}
     \mat{\rho} = \mat{\rho}^{\H} = (\mat{\rho}^{\T})^*.
   \end{gather*}

   From fundamental properties of linear algebra, this means that $\mat{\rho}$ has real
   eigenvalues, and one can choose the eigenvectors to form an orthonormal basis.  This
   means that $\mat{\rho} = \mat{U}\mat{D}\mat{U}^\H$ can be [diagonalized]:
   
   \begin{gather*}
     \mat{\rho} = 
     \overbrace{
     \begin{pmatrix}
       \uvect{u}_0 & \uvect{u}_1 & \cdots & \uvect{u}_{N-1}
     \end{pmatrix}
     }^{\mat{U}}
     \overbrace{
     \begin{pmatrix}
       \lambda_0\\
       & \lambda_1 \\
       & & \ddots \\
       & & & \lambda_{N-1}
     \end{pmatrix}
     }^{\mat{D}}
     \overbrace{
     \begin{pmatrix}
       \uvect{u}_0^\H \\
       \uvect{u}_1^H\\
       \vdots \\
       \uvect{u}_{N-1}^\H
     \end{pmatrix}
     }^{\mat{U}^\H},\\
     \mat{U}^\H \mat{U} = \mat{U}\mat{U}^\H = \mat{1}, \qquad
     \mat{D} = \diag(\lambda_0, \lambda_1, \cdots, \lambda_{N-1}), \qquad
     \lambda_n \in \mathbb{R},
   \end{gather*}
   
   where the columns of the [unitary matrix] $\mat{U}$ are the orthonormal eigenvectors,
   and the corresponding eigenvalues $\lambda_n$ are real.
   
2. **Unit trace**: The [trace] of the matrix is the sum of the diagonals, but can also
   be shown to be the sum of the eigenvalues:
   
   \begin{gather*}
     \Tr\mat{\rho} = \sum_{n} [\mat{\rho}]_{nn} = \sum_{n} \lambda_n = 1.
   \end{gather*}
   
3. **Positive semi-definite**: This means that the eigenvalues are not negative:
   
   \begin{gather*}
     0 \geq \lambda_n.
   \end{gather*}
:::

:::{margin}
The density matrix $\mat{\rho}$ is just the matrix of numbers obtained by expressing the density
operator $\op{\rho}$ in some orthonormal basis:

\begin{gather*}
  [\mat{\rho}]_{mn} = \braket{m|\op{\rho}|n}.
\end{gather*}
:::
The relationship between a pure quantum state $\ket{\psi}$ and the corresponding density
operator $\op{\rho}$ is just given by the outer product:

\begin{gather*}
  \op{\rho} = \frac{\ket{\psi}\!\bra{\psi}}{\braket{\psi|\psi}}.
\end{gather*}

:::{margin}
In some sense, the density matrix is more fundamental than the states
$\ket{\psi}$ since the phase and normalization of the state in the **full** Hilbert
space is not physically observable:

\begin{gather*}
  \ket{\psi} \equiv z\ket{\psi}, \qquad z\in \mathbb{C}.
\end{gather*}

The complex number $z$ drops out of the density operator:

\begin{gather*}
  \op{\rho} = \frac{z\ket{\psi}\!\bra{\psi}z^*}{\braket{\psi|z^*z|\psi}}
            = \frac{\ket{\psi}\!\bra{\psi}}{\braket{\psi|\psi}}.
\end{gather*}
:::

The density matrix provides a natural and complete way to describe a portion of
"the universe" that is separated (isolated) from the reset of the universe.  We will
call this portion our **system** and the rest of the universe **the environment**.  If this
system has always been isolated, then we can describe it by such a pure state, in which
case it will have [rank] 1, with a single eigenvector in the direction of $\ket{\psi}$
with eigenvalue $\lambda_{\psi} = 1$ and all other eigenvalues 0.

However, if the system interacted with the environment in the past, then it is possible
that the interactions generated **[entanglement]**, in which case multiple eigenvalues
will be non-zero.

:::{margin}
The situation is somewhat more complicated.  The entanglement entropy $S$ does measure
entanglement if the "state of the universe" is pure.  This is sometimes called
"bi-partite" entropy, and using the [Schmidt decomposition], one can show that the
entanglement entropy of the environment will also be $S$.

However, a positive $S$ might also denote a classical mixture of events who's
probability depends on the state of the environment.  One cannot distinguish between
these by considering only measurements on the system, but by looking for correlations
between measurements made on the **system** and independent measurements made on **the
environment** one can find results available within quantum mechanics that cannot be
realized classically. See [Bell's theorem] for details.
:::
One such measure of this entanglement is the Von Neumann [entanglement entropy]:

\begin{gather*}
  S(\mat{\rho}) = -\Tr[\mat{\rho}\log_{2}\mat{\rho}] \in [0,\log_2(N)].
\end{gather*}

For pure states, $S=0$.  Entanglement is a resource for quantum computing, and $S$
provides a (one of many) measure of this resource.

The most general state of a system can thus be expressed as a convex combination of pure
states:

\begin{gather*}
  \mat{\rho} = \sum_{i}\alpha_i\overbrace{\mat{\rho}_{i}}^{\ket{\psi_i}\bra{\psi_i}}, \qquad
  0\geq \alpha_i, \qquad
  \sum_{i} \alpha_i = 1.
\end{gather*}

The extremal points -- those density matrices which cannot be expressed as a convex
combination of distinct density matrices -- represent the pure states.  Interior points
have entanglement that might help quantum computers.

[Schmidt decomposition]: <https://en.wikipedia.org/wiki/Schmidt_decomposition>
[EPR]: <https://en.wikipedia.org/wiki/EPR_paradox>
[density matrix]: <https://en.wikipedia.org/wiki/Density_matrix>
[positive semi-definite]: <https://en.wikipedia.org/wiki/Positive-definite_matrix>
[Hermitian matrix]: <https://en.wikipedia.org/wiki/Hermitian_matrix>
[trace]: <https://en.wikipedia.org/wiki/Trace_class_operator>
[unitary matrix]: <https://en.wikipedia.org/wiki/Unitary_matrix>
[entanglement]: <https://en.wikipedia.org/wiki/Quantum_entanglement>
[rank]: <https://en.wikipedia.org/wiki/Rank_(linear_algebra)>
[entanglement entropy]: <https://en.wikipedia.org/wiki/Entropy_of_entanglement>
[Bell's theorem]: <https://en.wikipedia.org/wiki/Bell%27s_theorem>
