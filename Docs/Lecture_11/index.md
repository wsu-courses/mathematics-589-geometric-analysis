
# Lecture 11 Convexity, Legendre-Fenchel Transform, Duality

```{figure} Figures/example_figure.png
---
height: 350px
name: ex_fig
---
This is a caption of an example figure
```

Convexity:

Figure: convex function, blue well with
red step function

Jensen's inequality:

Goes here

Interpretation: take two points $x$ and $y$ in the domain and move along the line segement joining them; the function should always lie below this line.

Extended, the image of a sum is leq the sum of the images

Convex functions can be much more interesting than what is pictured above. Consider the following:

figure: two axes, function with kinks, its derivative would be a step function, but there is a generalized notion of a derivative: if a function is convex then the epigraph (all the points above the graph) f is convex if and only if epi(f) is a convex set.

With a closed epigraph, any point in the boundary is supported by an affine function (a hyperplane), that goes through that point and is strictly less than the graph.

We define two terms: subgradient and subdifferential. The subdifferential is the (a set valued function) collection of all the subgradients. A subgradient is the gradient associated with any supporting hyperplane at that point.

Convex sets and separation:

figure: convex oval filled in

In finite dimensions, if you have a point in a convex set you can always find a hyperplane that separates.

All linear maps from my space to the real numbers can be expressed as a dot product; if $h: x\to h(x)$ and $h$ is linear and continuous then we can always write $h(x) = <y_h,x>$. Where $h$ are the dual vectors; "blank" representation theorem?

What it means to have a separating hyperplane:

A hyperplane $h$ separates $C_1$ and $C_2$ if $h(x) \geq h(y)$ for all $x \in C_1$ and $y \in C_2$. What we have drawn are level sets of $h$, which we know live in one dimensional lower.

Suppose $C_1$ did not include its boundary, call it (the open egg) $C_2$, then the level set must go through the point indicated on the boundary.

## Differences Between Finite and Infinite Dimensional Cases

(Demonstration on why infinite dimensional cases are different - start w/ separable hilbert space, kicker is that we will have a convex guy that is not separable)

Figure: take a map from R to R; we want to know how often a convex function can turn. Tkae the dirac-delta measure centered on alpha. If the set contains alpha its measure is one, otherwise it's zero.

Let $R = \mathbb{Q} \cap (0,1)$. We know this set is countable since it's a subset of a countable set.

Now define a measure $h := \sum 1/2^i \delta_{q_i}$. Now define $g(x) := \int_0^x dh = \mu([0,x])$. Notice that we will jump every time we hit $1/2^i$.

Now define $w(x) := \int_0^x g(y) dy$. $w(x)$ is convex as its derivative is monotonically increasing. However, it has discontinuities at every rational point in $(0,1)$.

This function is differentiable at every irrational point (prove using cone property) and there is also a theorem that says a convex function is differentiable almost everywhere.

## Legendre-Fenchel Transform

Figure: convex guy with red axes, blue curve is f(x) and the legendre transform is f^*(k).

Let $k$ be the gradient. Draw the graph of one of the dual vectors; a green hyperplane. We can ask for the point in our domain such that: .... trailed off

\begin{gather*}
\f^*(k) := \sup_x (<k,x>-f(x))
\end{gather*}

is the point where we maximize the difference (red vert line), the red vert line is the distane we need to drop the green line for it to become a supporting hyperplane. Hence $<k,x> - f^*(k)$ is a supporting hyperplane of the blue curve.

If we take all the supporting hyperplanes and sup over them, we just get the function back. Thus for closed convex guys we have $f = \sup_k (<k,x> - f^*(x))$, thus the dual of the dual $f^{**} = f$ of $f$ is convex.

aside from earlier: given <k,x> if you hold k constant and vary x then you will get a plane through the origin

figure: higher dimensions, function is 0 on the red set and infinity everywhere else; lets see what the legendre transform is;

Imagine the hyperplane through the set (unit disk, pictured) we are finding the maximum distance between blue and red, which will be in the direction of the gradient on the edge (pictured in green) that height is the length of $k$. Thus

\begin{gather*}
f^*(k) = |k|
\end{gather*}

This says the legendre-fenchel dual is just this function (pictured in green) the absolute value function, looks like a cone

Physics Applications:

You can write the quantum state of a system as a vector $|\psi>$ and we use this notation for the conjugate transpose: $<\psi|$. 

Then we can write the inner product of two vectors $<a,b>$ as $<a|b>$.

Suppose we have an isolated system; we can describe it either as a state $|\psi>$ or as $\rho = |\psi \times \psi |$ which is the product of two matricies, and is called a density matrix.

If it is totally isolated we believe the above is the proper formulation

Density matrices are Hermitian with nonnegative eigenvalues. The trace of the matrix is 1; encapsulating the fact that our probabilities are conserved - it has to be somewhere.

We can form a quantity called the entanglement entropy:

\begin{gather*}
s = -Tr(\rho log_2\rho) \in [0,1]
\end{gather*}

\begin{gather*}
\rho = \begin{bmatrix}
\frac{1}{2} & 0\\
0 & \frac{1}{2}
\end{bmatrix}
\end{gather*}

This entanglement is one of the things that provides powers to quantum computers; if we have entanglement we can do new things. Hence it's important to categorize this.

We want to understand when things are and arent entangled, we understand these things in terms of convexity.

\begin{gather*}
\rho_0 = |0\times0| \rho_1 = |1\times1|\\
\rho = \alpha \rho_1 + (1-\alpha) \rho_1
\end{gather*}

This represents entanglement unless $\alpha$ is 1 or 0.

We can also write it as a convex sum as before.

The pure states (extreme points) are not linear combinations of anything; the states on an edge represent an entanglement between pure states.

These convex shapes with the kinks around the boundaries are easy to visualize in low dimensions; one subtlety is that the pure states are not always these kinks; consider a baseball with ridges; there are no isolated pure states there; there are these ridges here of lower entanglement.

About Legendre-Fenchel, thermodynamic ensambles.

In a really simple one dimensional context; imagine a simple gas, ignoring temperature. Typically we describe this by having a volume, we put the gas into this volume and there will be a certain number of particles there and an associated energy.

The variables we use are $n$ = density = particles \ vol

and energy density = energy \ vol

if we understand this guy we are interested in extensensivity; if we double its size then the energy is twice as much, particle size is twice as much, etc;

this does not hold for long term interactions like gravity

we can completely describe the system in the equation of the state; energy density as a function of density: $\varepsilon(n)$.

(figure: $\varepsilon$ vs $n$)

Above: imagine we put our guys in some configuration and look at the plot; we try some other arrangement, maybe this time a crystal arrangement and get another curve, etc.

In quantum mechanics the system wants to go to the lowest energy guy given some constraint(s)

so we take a box; throw into that box enough particle so that $n_0 = \frac{N}{V}$ and we want to know what happens.

What the system actually does is that it can phase separate and get liquid in the bottom of the box and gas up top; let us divide the volume into some fraction $x$; the volume down in the blue is $\alpha v$ and the top is $(1-\alpha)v$

Then the energy of the system is:

\being{gather*}
E = (1-\alpha)V \varepsilon(N_1/(1-alpha)v) + \alphav \varepsilon (N_2/\alpha V)
\end{gather*}

What ends up happening is that we build a new curve (indicated in red) that makes our guy convex; we endup at the red point between which is a mixture of the two low points of the curves.

Here's how we should look at it:

look at the derivative of energy:

$\mu = \varepsilon'() = E'(N)$

What we want is the Legendre-Fenchel transform:

$\rho(\mu) = \mu n - \varepsilon(n)$ which turns out to be the pressure;

we look at $\varepsilon(\mu)$.

Figure: plot of pressure $\varepsilon(\mu)$. We want to find the state that has max pressure for a given chemical potential.

We would like to be somewhere on the squggily curve and it has a kink at a point.

As we add more and more particles the blue line in the figure before moves up and up and up until the point where it is all liquid. 

We start at a low density phase; then we form equilibrium where we support another phase, then we fill up on that phase; etc.

further reading: rockafeller and wets "variational analysis"


```{toctree}
---
maxdepth: 2
titlesonly:
hidden:
glob:
---
DensityMatrices
:::