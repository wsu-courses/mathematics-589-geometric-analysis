# Lecture 3: Spaces: the Zoo of Countries where Everything Happens

![](Figures/spaces.png)

In this chapter we will meet a number of new spaces in which we will work and play. Recall that a space is a set equipped with additional structure; often another set.

## Measure spaces

In mathematics we are often concerned with measuring the length, area, or volume of a set. For example, consider the length of the set $\{ x | 1 \leq x \leq 3 \}$ of real numbers between and including 1 and 3.

![](Figures/numberline.png)

To calculate the length of the above set we simply subtract the end points; $3 - 1 = 2$ so the length of the set is 2. We can, following similar logic also calculate the area of a square or the volume of a cube, etc. 

However, there are sets we encounter in mathematics where it is not clear how we would even go about calculating their volume or if such a thing is possible at all. To address these cases we need more general tools, and so we will start by generalizing the notion of measuring things.

We will regard length and area as they are commonly understood to merely be special cases of **volume** which is the natural terminology in higher (3+) dimensional spaces - if we must specify for clarity we may refer to 1-dimensional or 2-dimensional volume instead.

To develop the theory of measuring sets we will first need to specify a set in which we are working: this could be the real numberline, the real plane, some arbitrary collection of points, etc. We then must develop rules that tell us which subsets can be reasonably assigned a volume - this is key as we want the volume of sets to work as we expect. Finally, we must construct a mathematical analog of a ruler called a measure which is a function that assigns numbers to the measurable subsets. A precise definition follows.

````{prf:definition}
:label: measureable_spaces

A **measurable space** (sometimes called a Borel space) is a pair $(M, \sigma)$ where $M$ is a non-empty set and $\sigma$ is a collection of subsets of $M$ satisfying the following properties:

1. $M$ is in $\sigma$
2. If a set is in $\sigma$, then so too is the compliment of that set.
3. If two sets are in $\sigma$ then so too is their union and intersection.
````

The set $\sigma$ is commonly called a $\sigma$-algebra and the elements of $\sigma$ are called measureable sets. Note that in practice we do not specify by hand the contents of $\sigma$. Rather, it is a best practice to link our structures together - we very often for example want to have a measureable space, equipped with a topology and a metric. It makes sense then for us to make a single choice and inherit as much structure as we can from it. For example, we might specify the metric - which we will use to define a topology which will in turn be used to define an appropriate $\sigma$-algebra. 

Now that we've defined which sets are measurable, we will define our measuring tool.

````{prf:definition}
:label: measure_spaces

A measure space is a triple $(M, \sigma, \mu)$ where $(M, \sigma)$ is a measurable space and $\mu$ is a function $\mu: M \to \bar{\mathbb{R}}$  (called a measure) which satisfies the following:

1. For any measurable set $E \in \sigma$, $\mu(E) \geq 0$.
2. $\mu(\emptyset) = 0$.
3. If $A,B \in \sigma$ are measureble sets with no elements in common (they are disjoint) then $\mu(A \cup B) = \mu(A) + \mu(B)$.
````

```{margin}

Stop and think: Show by induction that for a countable collection of pairwise disjoint sets $\{E_i \}_{i=1}^\infty$

\begin{gather*}
\mu \left( \bigcup_{i=1}^\infty E_i \right) = \sum_{i=1}^\infty \mu(E_i)
\end{gather*}
```

Where $\bar{\mathbb{R}}$ is the *extended real numberline* which is $\mathbb{R} \cup \{ -\infty,\infty \}$ with the symbols $-\infty,\infty$ satisfying the following inequality for all $x \in \mathbb{R}$:

\begin{gather*}
-\infty < x < \infty
\end{gather*}

## Measurable Functions

Just as the structure preserving maps of vector spaces are linear functions and the structure preserving maps of topological spaces are continuous maps, the structure preserving maps of measurable spaces are measurable functions. 

Where or not a function is measurable does not depend on the measure we wish to use. Rather, whether or not a function is measurable depends only on how it treats measurable sets. A precise definition follows:

````{prf:definition}
:label: measureable_functions

Let $(A, \sigma_A)$ and $(B, \sigma_B)$ be measurable spaces. A map $f: A \to B$ is **measurable** if the preimages of measurable sets are measurable.
````

That is, if we have a subset of the range which is measurable (in $\sigma_B$) then the collection of points in the domain that map into it must be measurable (in $\sigma_A$).

Note that the above definition is precisely that of continuous maps, with "continuous" replaced with "measurable". In fact, if we inherit a $\sigma$-algebra from topologies established on the domain and range then any continuous map is indeed measurable.

## Integration of measurable functions

We now approach the major payoff of developing measure theory. We will use our work to define the notion of *Lebesgue integration*, a far more powerful type of integration than the standard Riemannian Integration taught in calculus courses. 

This will allow us to integrate measurable functions against a measure of our choosing - this is extraordinarily powerful as later on we will craft measures that are tailored to making our problems as easy as possible. Furthermore, utilizing the generality of measurable functions we will be able to integrate functions that we couldn't otherwise with Riemannian integration. Lastly, all of our knowledge in learning Riemannian integration will not go to waste as Lebesgue integration will yield all the familiar results we are used to. 

We proceed to define Lesbegue integration in 3 steps, first we will define the Lesbegue integral for so-called simple functions, then we will define Lesbegue integration for non-negative measureable functions and then finally we will extend to general functions.

Recall from your introduction to Riemannian integration that if we want to integrate a function over an interval $[a,b]$ we begin by partitioning an interval into rectangles that approximate the function.

![](Figures/riemann_integral.png)

The idea of Riemannian integration is that if we sum the area between this approximating function and the $x$-axis we get an approximation for the area between the curve and the $x$-axis. As we partition into more and more rectangles our approximation becomes more accurate and in the limit as the number of rectangles goes to infinity our approximation and the true value are equal.

Keeping this picture in the back of our minds we will now develop Lebesgue integration.


````{prf:definition}
:label: char_functions


Let $M$ be a set and let $A$ be a subset of $M$. We define the **characteristic function** (sometimes called indicator function) of $A$ denoted $X_A: M \to \{0,1\}$ as follows:

\begin{gather*}
X_A(x) = \begin{cases} 
      1 & \text{ if } x \in A\\
      0 & \text{ otherwise}
   \end{cases}
\end{gather*}

````

The characteristic function simply tells us if a given element $x$ of a parent set $M$ is in a particular subset $A$ by returning a $1$ if it is and a $0$ if it is not.

For example, let $M = \mathbb{R}$ and $A = [0,1]$. Then we can plot $X_A(x)$ as follows:

![](Figures/indicator_function.png)

Characteristic functions will serve as the building blocks for our analog of function approximations. We will now connect these concepts via simple functions.

````{prf:definition}
:label: simple_functions

A function $f: M \to \mathbb{R}$ is a **simple function** if it can be written as a linear combination of characteristic functions:

\begin{gather*}
f(x) = \sum_{i=1}^n a_iX_{A_i}(x)
\end{gather*}

Where $\{A_i\}_{i=1}^n$ are disjoint subsets of $M$ and the coefficients $\{a_i\}_{i=1}^n$ are real numbers.

````

Simple functions are easy to work with as the name suggests. For example, let $A = (0,1)$, $B = [1,2)$ and $C = [2,3)$. Consider the simple function:

\begin{gather*}
S(x) = X_A(x) + 2X_B(x) + 3X_C(x)
\end{gather*}

```{margin}

Stop and think: How would you define the integral of the pictured function with respect to some measure $\mu$?
```

![](Figures/simple_function.png)

````{prf:definition}
:label: simple_int

Let $(M,\sigma,\mu)$ be a measure space and $s(x) = \sum_{i=1}^n r_i X_{A_i}(x)$ be a nonnegative measurable simple function. Then we define the **integral of $s$ over $M$** to be:

\begin{gather*}
\int_M s d\mu := \sum_{i=1}^n r_i \mu(A_i)
\end{gather*}

````

The reason why we specify a non-negative simple function is because the measure of a set $\mu(A_i)$ may be $\infty$ and $\infty - \infty$ is undefined.

We can now define the integral of a general non-negative measureable function.

````{prf:definition}
:label: nonneg_int

Let $(M,\sigma,\mu)$ be a measure space and let $f: M \to \bar{\mathbb{R}}$ be a nonnegative measurable function. Let $S$ denote the set of all non-negative measurable simple functions $s: M \to \mathbb{R}$ such that $s(x) \leq f(x)$ for all $x \in M$. Then we define:

\begin{gather*}
\int_M f d\mu := \sup_{s \in S} \int_M s d\mu
\end{gather*}

````

Alternatively we may also write:

\begin{gather*}
\int_M f(x) \mu(dx) = \int_M fd\mu
\end{gather*}

As it is often easier to write in a formula for $f$ in terms of a dummy variable $x$ rather than refering to $f$ by its name.

````{prf:definition}
:label: leb_integrable

Let $(M,\sigma,\mu)$ be a measure space and let $f: M \to \bar{\mathbb{R}}$ be measurable. $f$ is Lebesgue integrable if:

\begin{gather*}
\int_M |f|d\mu < \infty
\end{gather*}
````

We denote the set of all Lebesgue integrable functions over a set $M$ by $\mathcal{L}^1(M)$.

````{prf:definition}
:label: leb_int

Let $(M, \sigma, \mu)$ be a measure space and let $f: M \to \bar{\mathbb{R}}$ be Lebesgue integrable. Then the Lebesgue integral of $f$ over $M$ with respect to the measure $\mu$ is given by:

\begin{gather*}
\int_m f d\mu := \int_M f^+ d\mu - \int_M f^- d\mu
\end{gather*}

Where $f^+ := \max(f,0)$ and $f^- := \max(-f,0)$.

````

From now on, if we say "integrable" or "integral" we mean "Lebesgue integrable" and "Lebesgue integral".

Given some integrable function $f$ it is difficult to actually calculate its integral based on the supremum definition above; however it can be shown that there exists a monotone sequence $\{s_n\}_{n=1}^\infty$ of non-negative measurable simple functions (that can be explicitly constructed from $f$) whose limit is $f$ and we have:

\begin{gather*}
\int_M f d\mu = \lim_{n \to \infty} \int_M s_n d\mu
\end{gather*}

and the right-hand side can generally be evaluated easily.

## Spaces of functions

### $L^p$ spaces

We will now define and discuss one of the most important classes of Banach spaces in functional analysis.

Let $(X,\mathcal{A},\mu)$ be a measure space and $p \geq 1$. Define the following set of functions:

\begin{gather*}
L^p(\mu) := \left\{ f : \int |f|^p d\mu < \infty \right\}
\end{gather*}

If we define $\vert\vert f \vert\vert_p = \left( \int |f|^p d\mu \right)^{\frac{1}{p}}$ and regard functions that differ only on a set of of measure 0 as the same then $(L^p(\mu), \vert \cdot \vert_p)$ is a Banach space.

Of particular interest is $L^2(\mu)$ spaces wherein

\begin{gather*}
\langle f, g \rangle = \int f g d\mu
\end{gather*}

Defines an inner product and thus $L^2(\mu)$ is a Hilbert space.

L^\infty

### Sobolev spaces

Sobolev spaces are key tools in solving differential equations. Essentially, they combine the concepts of weak differentiability and $L^p$ norms to create a Banach space relevant to a given problem.

On the real line, the Sobolev space is denoted by $W^{k,p}(\mathbb{R})$ and is the collection of functions and their weak derivatives up to order $k$ with a finite $L^P$-norm $\vert \vert \cdot \vert \vert_{L^p}$. We can equip $W^{k,p}(\mathbb{R})$ with the norm:

\begin{gather*}
\vert \vert f \vert \vert_{k,p} = \left( \sum_{i=0}^k \int |f^{(i)}(t)|^pdt \right)^\frac{1}{p}
\end{gather*}

which turns it into a Banach space.

Like $L^2$, $W^{k,2}$ is of particular interest because it admits an inner product, which is defined in terms of the $L^2$ inner product:

\begin{gather*}
\langle f,g \rangle_{W^{k,2}} = \sum_{i=0}^k \langle D^i f, D^i g \rangle_{L^2}
\end{gather*}

Which makes $W^{k,2}$ a Hilbert space.