<!-- (Point For Week 9: tbd) -->

# Lecture 9: Inequalities, Geometrically: Concentration of Measure in High Dimensions, Markov, Isoperimetric, Jensens, AM-GM and Cauchy-Schwarz

![](Figures/scholarly.png)

```{figure} Figures/example_figure.png
---
height: 350px
name: ex_fig
---
This is a caption of an example figure
```

:::{sidebar} Example Topic

This is an example of a sidebar.
:::

Today: inequalities and geometry

In geometric analysis, inequalities are a big deal. Many of our definitions rely on inequalities. For example, recall the definition of continuity of a function at a point; we require that $|f(x)-f(y)| < \varepsilon$ whenever $|x-y| < \delta$.

This is where a lot of things start from; we don't care precisely about the value of $|f(x)-f(y)|$, only that it's bounded and we can make it as small as we like.


## Concentration of Measure

book recommendation: Michel Ledoux; "The concentration of measure phenomenon"

We'll start with the volume of balls in arbitrary dimensions.

(Drawing of a unit ball in R^N)

Denote the $n$-volume of a unit ball centered on the origin $B(0,1)$ as $\alpha(n)$. If $n$ is 1 then $\alpha(1) = 2$, $\alpha(2) = \pi$, $\alpha(3) = \frac{4}{3} \pi$. As the dimension grows, the volume of said ball tends towards 0.

This begs the question: Where is the volume in $B^n(0,1)$ when n gets really big? The answer may be a bit surprising; the volume is actually concentrated in the surface, which we can show by calculation:

Note: $H^n(B(0,r))) = \alpha(n)r^n$ shows that the measure of the ball scales like the $n$th power of the radius.

This implies that

\begin{gather*}
H^n(B(0,1-\varepsilon))/H^N(B(0,1)) = (1-\varepsilon)^n
\end{gather*}

This says that the outer $\varepsilon$ shell of the ball contains almost all the volume as n goes to infinity. In low dimensions this may not matter much; but in high dimensional spaces (and high dimensional representations of low dimensional spaces) this matters a lot. This pops up a lot in statistical learning theory; aka the foundations of machine learning.

Now we'd like to ask:

(Picture: sphere with equator and dotted line in the back)

Suppose we have a uniform probability measure on the surface of the unit ball in $n$ dimensions $\partial B^n(0,1)$. What can we say about the distribution of points?

In our drawing we have a great circle; it is defined by the indicated normal vector, we look for all points on the sphere orthogonal to that vector.

Q: How much probability is concentrated around said great circle?

A: Take a little band using the notion distance that makes sense on the sphere, the geodesics.

This little band represents the geodisic $\varepsilon$ neighborhood of the great circle, i.e. the distance between them is 2 $\varepsilon$. So we've normalized the measure of area on the sphere so that the total area is equal to 1, and it's also invariant under rotations. We'd now like to get a handle on how much stuff is here.

Recall the area of a graph;

Figure: generic surface

If we want to know the area of the surface, we know from calculus:

\begin{gather*}
\text{Area} = \int_\omega \sqrt(1+\nabla f \cdot \nabla f) dx
\end{gather*}

Using this idea we make the following argument:

Figure: section through the sphere and a section through that plane to cut it.

The idea is this: we have (an exaggerated) band and we will think about how that band divides the sphere. If we rotate the band a little bit we have a red disc and inside of that we have a green disc and the green disc is underneath the part not covered by the band (project straight down) and we get an annulus.

Note the integral $\sqrt{1+\nabla f \cdot \nabla f}$. For every point in the band the gradient of the function is bigger than the uncovered part.

When we're integrating the only thin that is changing is the gradient; so the thing that turns the flat areas into the curved areas are that quantity. Hence the area of the red divided by the area of the green is less than the area of the yellow divided by the area of the blue.

Furthermore; we want the ratio of the yellow to the whole things vs the ratio of the red to the whole thing.

This argument yields the following:

\begin{gather*}
H^{n-1}(\varepsilon \text{band})/H^{n-1}(\text{upper hemisphere}) \geq H^{n-1}(\text{red annulus})\H^{n-1}(\text{unit n-1 disc})
\end{gather*}

(note disc = ball)

We already know that if the indicated distance is epsilon then $H^{n-1}(\text{red annulus}) = 1-(1-\varepsilon)^n$ and the denominator is equal to 1 (normalized) and this ratio is $1-(1-\epsilon)^n$.

So we know that $(1-\varepsilon)^n \leq e^{-\varepsilon n}$.

Why? Let's draw a picture.

Figure: exponential function

Draw the line $1+x$. We can see that $1 + x \leq e^x$. If we stick in $-\varepsilon$ then we see:

\begin{gather*}
(1-\varepsilon)^n \leq e^{-\varepsilon n}
\end{gather*}

Which was what we wanted.

So we can do some substituting to show the ratio we are interested in is larger than:

\begin{gather*}
\geq 1 - \varepsilon^{-\varepsilon n}
\end{gather*}

Figure: circle with tangent line at the bottom

We want to know the ratio of the yellow to the vertical red. For the unit sphere we have:

\begin{gather*}
x^2+y^2 = 1\\
y = -\sqrt{1-x^2}
\end{gather*}

And we can shift it up by 1:

\begin{gather*}
y = -\sqrt{1-x^2} + 1
\end{gather*}

If $x$ is small:

\begin{gather*}
\approx 1 - (1-\varepsilon^2/2)\\
= x^2/2
\end{gather*}

So if $y = 1\2x^2$ and we want to integrate it from $0$ to $\varepsilon$ we want:

\begin{gather*}
\int_0^{\sqrt{2}\varepsilon} \sqrt{1 +x^2}dx = \sqrt{2}\varepsilon
\end{gather*}

So if we use $\delta = \sqrt{2}\varepsilon$ then the ratio we care about is $\geq 1-e^{-\delta^2/2}$

We end up with that the probability of the 2\varepsilon band is \geq 1-e^{-\varepsilon^2/2 n}

So as n gets huge the probability (read: measures) gets as close to 1 as we want.

## Isoparametric Inequalities on Spheres

Suppose we have a set on the sphere that contains half the $n$-1 volume.

Figure: sphere with sets on it

The sets together in our figure make up half the surface area.

When is the ratio of $H^{n-2}(\partial E)/H^{n-1}(E)$ smallest?

If we make our set into a really long snake we can make the n-2 dimensional area as big as we want; we can't however make it as small as we want.

The shape for the minimal one is merely half a ball - a hemisphere.

If we take the boundary of a hemisphere (which is a great circle) that will have the smallest $n$-2 area we can have.

(insert)

If we have a set that contains exactly half the area of the sphere; then we know that when we expand it by $\varepsilon$ (take all the points within $\varepsilon$ of the set) the rate at which the area is growing of that set is at least as fast as if we did the same thing to a hemisphere.

(May need to relocate the above)

So if we keep the volume fixed and move the set we get a hemisphere.

If we have $E$ we can think about $E_\varepsilon$ which is $E + B_\varepsilon$ the set E plus the $\varepsilon$ ball (geodesic ball in the surface)

One of the consequences:

If $f: \partial B(0,1) \to \mathbb{R}$

With $n$ big; then $f$ being lipschitz implies $f$ is almost constant.

Figure: function over a blue set, meant to demonstrate median value and the idea of being almost constant.

Figure: sphere

Imagine on the sphere we draw a red set where the function is less than or equal to the median value, what is left is greater, since its continuous the curve is where its equal

The length is at least as long as the hemisphere

If we move epsilon out or in from the curve if we are in really high dimensions that region has almost all of the volume of the sphere.

If we give the curve a name (call it $c = where f = \bar{f})$ so $H^{n-1}(C_\varepsilon)/H^{n-1}(\partial B(0,1)) \approx 1$.

Since we know $f$ is lipschitz; i.e.:

\begin{gather*}
|f(x)-\bar{f}| \leq k \varepsilon
\end{gather*}

Where $k$ is the Lipschitz constant. So we get that the function is approx constant as we get within $\epsilon k$.

Further reading: David Donoho, blessings and cursings of high dimensions

## Markov

Figure: Remembering this inequality; arbitrary green function with blue axes

We consider an arbitrary function and consider a level $y$. We then draw the level through the function and project down to find those values that are mapped to values less than or equal to $y$.

The statement is:

$y$ times the measure of the set $x$ where $f(x)$ is greater than or equal to $y$ is itself less than or equal to the area under the curve above it.

i.e.

\begin{gather*}
\mu(x : f(x) \geq y) \leq \int_\omega |f| dx / y
\end{gather*}

Application:

Probability. The probability of the set $x$ such that:

\begin{gather*}
|f(x)-\bar{f}|^2 > \delta \leq \int |f(x)-\bar{f}|^2/\delta^2
\end{gather*}

The probability of deviating of the mean more than delta is just the variance divided by your delta squared.

This is used a lot in machine learning.

Notes on machine learning: Rob Nowak, at Wisconsin, statistical learning theory - check out his website

Jensens:

Figure: Convex function.

Line segment lies above the graph.

Geometrically equivalent to the following statement:

\begin{gather*}
f(\alpha x + (1-\alpha)y) \leq \alpha f(x) + (1-\alpha)f(x)
\end{gather*}

Where $\alpha$ is between 0 and 1.

\begin{gather*}
f(\sum_{i=1}^N \alpha_i x_i) \leq \sum_{i=1}^N \alpha_if(x_i)
\end{gather*}

AM-GM: (arithmetic mean, geometric mean)

(this is a nightmare to typeset on the fly... refer to kevins notes later, it's a bunch of betas)

The geometric mean is always less than or equal to the arithmetic mean

We can use Jensens to prove this.

easy, log proof in kevins notes

Isoperimetric inequalities:

If we have a set $E \subset \mathbb{R}^N $ then the volume of the boundary raised to the power of $n/(n-1)$ divided by the volume of $E$ is greater than or equal to the same ratio for $E = B(0,1)$.

Expounding:

Volume of ball is $\alpha(n)r^n$ implying the surface area is the derivative of the former.

If we expand a ball the amount you expanded by is the surface area of the ball times the length you expanded by.

(Do up the fraction for E=B(0,1), refer to kevins notes)

We write down the isoperimetric inequality in $n$ and in $2$ dimensions; grab it from notes.

Figure: Manifold / deformed sphere

<!-- Below denotes the location of a footnote -->

[^1]

```{toctree}
ConcentrationOfMeasure
```
