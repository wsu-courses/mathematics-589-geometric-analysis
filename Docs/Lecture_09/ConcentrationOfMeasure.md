---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (math-589-2022)
  language: python
  name: math-589-2022
---

```{code-cell} ipython3
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

## Examples of Measure Concentration

### Gaussian Distribution (and chi-squared)

Here we demonstrate the concentration of measure by looking at an $D$-dimensional
gaussian distribution with zero mean and unit variance:

\begin{gather*}
  P(\vect{x}) = \frac{1}{\sqrt{(2\pi)^{D}}}e^{-\norm{\vect{x}}^2/2} 
              = \frac{1}{\sqrt{(2\pi)^{D}}}e^{-r^2/2}.
\end{gather*}

Now we can consider the distribution of probability as a function of
$r=\norm{\vect{x}}$.  This is obtained from $P(\vect{x})$ by performing the angular
integrals $\int\d\Omega$ and including the metric:

\begin{gather*}
   \d{\vect{x}}^D = r^{D-1}\d{r}\d{\Omega}, \\
   P_D(r) = r^{D-1}\int\d{\Omega}\;P(\vect{x}) = S_{D-1} r^{D-1}P(r),\\
   S_{D-1} = \frac{2\pi^{n/2}}{\Gamma\left(\frac{D}{2}\right)}.
\end{gather*}

The factor $S_{D-1}$ is just the surface "area" of a unit [$D$-dimensional
sphere](https://en.wikipedia.org/wiki/N-sphere).  The key to the concentration of
measure is the factor $r^{D-1}$:

```{margin}
Properly normalized, $P_D(r)$ is just the famous [chi-squared
distribution](https://en.wikipedia.org/wiki/Chi-squared_distribution) with $x=r^2$ and
$k = D$:

\begin{gather*}
  P_D(r) = P_{\chi^2}(r^2, D).
\end{gather*}
```
\begin{gather*}
  P_D(r) \propto r^{D-1} e^{-r^2/2}.
\end{gather*}

```{code-cell} ipython3

fig, ax = plt.subplots()
r = np.linspace(0, 15, 500)

Ds = [1, 2, 3, 4, 5, 10, 50, 100]
for D in Ds:
    P = r**(D-1) * np.exp(-r**2/2)
    ax.plot(r, P/np.trapz(P, r), label=f"$D={D}$")
ax.set(xlabel="r", ylabel="$P_r(r)$")
ax.legend();
```

Notice that the mean increases.  In fact, one can show that the mean is at $\sqrt{D-1}$,
so we can scale the abscissa and ordinate by this factor to collapse these distributions while
maintaining the unit area property:

```{code-cell} ipython3

fig, ax = plt.subplots()
r = np.linspace(0, 15, 500)

Ds = [2, 3, 4, 5, 10, 50, 100]
for D in Ds:
    P = r**(D-1) * np.exp(-r**2/2)
    ax.plot(r/np.sqrt(D-1), np.sqrt(D-1)*P/np.trapz(P, r), 
            label=f"$D={D}$")
ax.set(xlabel="$r/\sqrt{D-1}$", xlim=(0, 2.5),
       ylabel="$P_r(r)\sqrt{D-1}$")
ax.legend();
```

### Uniform Distribution

Here is a similar demonstration for a uniform distribution:

```{code-cell} ipython3

fig, ax = plt.subplots()
r = np.linspace(0, 1.1, 500)

Ds = [1, 2, 3, 4, 5, 10, 50]
for D in Ds:
    P = r**(D-1) * np.where(r<1, 1, 0)
    ax.plot(r, P/np.trapz(P, r), label=f"$D={D}$")
ax.set(xlabel="r", ylabel="$P_r(r)$")
ax.legend();
```
