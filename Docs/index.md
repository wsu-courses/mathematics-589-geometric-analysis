<!-- Math 589 - Geometric Analysis
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
-->

# Mathematics 589: Geometric Analysis with a View to Use 

```{toctree}
---
maxdepth: 2
caption: "Contents:"
titlesonly:
hidden:
glob:
---
Preface
Overview
Lecture_*/index
Syllabus
References
```

```{toctree}
---
maxdepth: 2
caption: "Sandbox:"
hidden:
glob:
---
Sandbox/*
```


```{toctree}
---
maxdepth: 2
caption: "Miscellaneous:"
hidden:
---
CoCalc
ClassLog
../InstructorNotes
README.md <../README>
```

<!-- If you opt to literally include files like ../README.md and would like to be able
     to take advantage of `sphinx-autobuild` (`make doc-server`), then you must make
     sure that you pass the name of any of these files to `sphinx-autobuild` in the
     `Makefile` so that those files will be regenerated.  We do this already for
     `index.md` but leave this note in case you want to do this elsewhere.
     
     Alternatively, you can include them separately and view these directly when editing.
     We do not include this extra toc when we build on RTD or on CoCalc.  We do this
     using the `sphinx.ext.ifconfig extension`:
     
     https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html

```{eval-rst}
.. ifconfig:: not on_rtd and not on_cocalc

   .. toctree::
      :maxdepth: 0
      :caption: Top-level Files:
      :titlesonly:
      :hidden:

      README.md <../README>
      InstructorNotes.md <../InstructorNotes>
```
-->


This book are the lectures given in a course that I gave with the assistance of Michael Forbes. The notes were collected from the students and then put together and edited by a smaller group of students -- Brian Becsi, Jared Brannan, Rommel Cortez, Curtis Michels, Harlan Heilman, and William Wills. The book is avaialable in hardcopy from here ...


