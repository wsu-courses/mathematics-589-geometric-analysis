---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (math-589-2022)
  language: python
  name: math-589-2022
---

```{code-cell} ipython3
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
import manim.utils.ipython_magic
```

# Lecture 6: Inverse Function Theorem, Implicit Function Theorem, and Sards Theorem: Just Three (Powerful) Examples of the Use of Derivatives

```{figure} Figures/example_figure.png
---
height: 350px
name: ex_fig
---
This is a caption of an example figure
```

-------
The goal here is to get a not-so-fuzzy feel for Manifolds and Function theorems. Through this exploration, we'll build the intutions necessary to pursue difficult calculations. For example, differential geometry is known for having calculations that can take months to complete and to carry them out you need a set of intuitions to guide you. Without these intutions, even the most patient explorer may be lost forever.

---
## Inverse Function Theorem
We start with the inverse function theorem: the idea is very simple. Take, for example, the system

\begin{gather*}
	y = Ax,\qquad A\in M_{n\times n}(\mathbb{R}),\qquad \det(A)\neq 0
\end{gather*}

then we know there's an inverse of $A$.

The same can be generalized for nonlinear functions by utilizing its local linear approximation:

\begin{gather*}
	y = F(x)
\end{gather*}

If $D_x F$ is a continuous and invertible map, then there exists an inverse of $F$, call it $F^{-1},$ at $(x, y)$. 

```{margin}
A proof of the inverse function theorem can be found elsewhere.

This theorem can be further generalized to aply to any Banach space.
```

(*figure of inverted arbitrary function*)

![](Figures/general_functions.png)

```{code-cell} ipython3
:tags: [hide-input]

%%manim --embed -v WARNING --progress_bar None -qm InverseFunction
from manim import *

my_tex_template = TexTemplate()
with open("../_static/math_defs.tex") as f:
    my_tex_template.add_to_preamble(f.read())

class InverseFunction(Scene):
    def construct(self):
        config["tex_template"] = my_tex_template
        config["media_width"] = "100%"
        class colors:
            ellipse = YELLOW
            x = BLUE
            y = GREEN
            r = RED
        ax1 = Axes(
            x_range=[-1, 4],
            y_range=[-1, 3],
        )

        ax2 = Axes(
            y_range=[-1, 4],
            x_range=[-1, 3],
        )
        
        def f(x, d=0):
            if d == 0:
                return (x-0.5)**3/3 - 3*(x-0.5)**2/4 + 1
            elif d == 1:
                return (x-0.5)**2 - 3*(x-0.5)/2
    
        x0 = 2.5
        dx = 0.5
        def df(x, x0=x0):
            return f(x0, d=0) + f(x0, d=1)*(x-x0)
            
        curve1 = ax1.plot(f , color=BLUE)
        line1 = ax1.plot(df, x_range=[x0-dx, x0+dx], color=GREEN)
        curve2 = ax2.plot_parametric_curve(lambda x: (f(x), x), t_range=ax1.x_range, color=BLUE)
        line2 = ax2.plot_parametric_curve(lambda x: (df(x), x), t_range=[x0-dx, x0+dx], color=GREEN)
        
        ax1_labels = ax1.get_axis_labels(x_label='x', y_label='y')
        ax2_labels = ax2.get_axis_labels(x_label='y', y_label='x')
        plot1 = VGroup(ax1, curve1, line1, ax1_labels)
        plot2 = VGroup(ax2, curve2, line2, ax2_labels)
        self.add(plot1)
        #self.add(plot2)
```


![](Figures/general_inverted_function.png)

Why need it be continuous? Well, consider a function who's derivative is not continuous:

![](Figures/discont_deriv.png)

If the derivative is not continuous, then $F^{-1}$ won't exist in a neighborhood of $(x,y)$.



### Inverse problems
In some sense, science is just one big inverse problem. We start with some measurements $y$ that come from a complicated state space.

\begin{gather*}
	y = F(x)
\end{gather*}
In other words, we record $y$ and we're after $x$.
It's imediate that this is the same system we had before, so the inverse function theorem is perfect for the task.

![](Figures/measurements.png)

When we measure things, we don't have exact points, but intervals of uncertainty. How much uncertainty we have in $x$ is completely dependent on the derivative (to first order). For example, if $D F(x_1) = 1/2$, then we have

\begin{gather*}
	\textrm{diam}(F^{-1}([y_1 - \varepsilon, y_1 + \varepsilon])) \approx 2 \textrm{ diam}([y_1 - \varepsilon, y_1 + \varepsilon]).
\end{gather*}

In this way, the inverse function theorem can give estimates for stability.

---
## Implicit Function Theorem
Now, for the implicit function theorem, consider maps 
\begin{gather*}
  G:\mathbb{R}^n \to \mathbb{R}^m,\ m < n.
\end{gather*}

First, an example: $G:\mathbb{R}^2 \to \mathbb{R}^1$

![](Figures/level_set_function.png)

If we draw the level sets of this function (which will be an $n-m$-dimensional surface), the function is constant on those level sets (by definition):

\begin{gather*}
	G(x,y) = C.
\end{gather*}

Then, the implicit function theorem states when there exists a function $f$ such that $f(y) = x$ on this level set. In other words, there's an $f$ such that $G(x, f(x)) = C$ around some goal point $(x^*, y^*)$.
Notice, this cannot be done if the tangent line to $G$ with respect to the $x$ axis is verticle.

More explicitly, the implicit function theorem says if $\frac{\partial}{\partial y}G(x^*,y^*) \neq 0$, then there eixists a function $f$ such that $f(y) = x$ around $(x^*, y^*)$.

```{margin}
This works in infinite dimensions.
```

The heart of this idea is the follwing. We know $G(x^*, y^*) = C$ and we want to move away from $(x^*,y^*)$ while staying on the same level set. Then, $G(x^* + \varepsilon, y^*) = C + \delta$, and we need to compensate for this $\delta$ by shifting $y^*$. If $\frac{\partial}{\partial y}G(x^*,y^*) = 0$, we have no freedom to move $y^*$, since shifting it around won't change the output of $G$. In other words, we're stuck at $G(x,y) = C + \delta$, which is the wrong level set. On the other hand, if $\frac{\partial}{\partial y}G(x^*,y^*) \neq 0$, we know that shifting $y^*$ by some $\delta'$, we will move away from our current position on the curve:

\begin{gather*}
  G(x^*+\delta, y^*+\delta') = C +\delta +\delta'.
\end{gather*}

This means we can always return to the level set we started on.

For the more general case, consider $x^*$ in $(n-m)$-dimensions, $y^*$ in $m$-dimensions, and $C$ $m$-dimensions. So, we need $m$ dimensions of control. I.e. we need an invertible continous linear operator between $y^*$ and $C$. Such an operator is a full rank continuous Jacobian of $G$.

In other words, if we have a nonzero derivative, then $G$ acts locally like an $(n-m)$-dimensional manifold (something that has open coordinate patches).

*Note for authors: look up an explicit definition to include*

---
## Full Rank Theorem
Suppose $G:\mathbb{R}^n\to\mathbb{R}^m,\ m < n$ and $y\in\mathbb{R}^m$ is a regular value, i.e. $DG$ is full rank on all of $G^{-1}(y)$, then $G^{-1}(y)$ is an embedded $(n-m)$-dimensional submanifold of $\mathbb{R}^n.$

Example: (*figure of the figure-8 level set*)

Full rank maps can be perturbed to maintain their full rankedness.

---
## Manifolds
*($n$-manifolds)*

```{margin}
Some nice references:
- *Functions of Several Variables*
  - Introduces manifolds using the implicit function theorem
- *Differential Topology* by Gillman and Polack
```

A manifold can be thought of as a subset of $\mathbb{R}^n$ that's smooth. Or, for a manifold $M$ and open subsets $U\subset M$ and $W\subset \mathbb{R}^n$, there exists a one-to-one corresponence for every point in $M$

\begin{gather*}
	\phi: U \to W,
\end{gather*}

We call $\phi$ a *coordinate map*. Then, for two coordinate maps $\phi_1$ and $\phi_2$, with open sets $U_1, U_2$, then on $\phi_1(U_1)\cap\phi_2(U_2)$, we can consider $\phi_2\circ\phi_1^{-1}$, called the *overlap map*.

(*figure from notes*)

For example, supoose we take a subset of $\mathbb{R}^2$ thats the boundary of a square:

(*square from notes*)

Then take the maps $\phi_x: M\to\mathbb{R}^1$ that maps intervals of width $2\delta$ on $M$ to intervals of width $2\delta$ in $\mathbb{R}^1$. Consider the overlap maps of $\phi_x$ and $\phi_y$: $\phi_y\circ\phi_x^{-1}$. This is just a translation, for every point on the square, which means the overlap maps are as smooth as you'd like. So, even though the manifold isn't smooth extrinsically, it is smooth intrinsically, even though it has corners.

### Submanifolds
*(submanifolds of $\mathbb{R}^n$)*


```{margin}
*Smooth diffeomorphism*: a $C^\infty$ function with an inverse that is also $C^\infty$
```

$M$ is a $k$-dimensional submanifold of $\mathbb{R}^n$ if for every $x\in M$ there's an open set $U\subset\mathbb{R}^n$ and a smooth diffeomorphism $F:\mathbb{R}^n\to\mathbb{R}^n$ such that

\begin{gather*}
	F(M\cap U) = W\cap\hat{\mathbb{R}}^k
\end{gather*}

where $W$ is an open neighborhood of the origin and $\hat{\mathbb{R}}^k = \{x\in\mathbb{R}^n \ \vert\ x_{k+1} = x_{k+2} = \cdots = x_m = 0\}$

(*figure of the curvy guy with $F(x) = 0$*)

Alternatively, we can say that $M$ is a $k$-submanifold of $\mathbb{R}^n$ if $M$ is locally the level set of a map from $\mathbb{R}^n\to\mathbb{R}^{n-k}$ that is full rank (Fleming takes this approach).

----

<!-- (Point For Week 6: tbd) -->

![](Figures/coffee.png)

```{figure} example_figure.png
---
height: 350px
name: ex_fig
---
This is a caption of an example figure
```

:::{sidebar} Example Topic

This is an example of a sidebar.
:::

```{margin}
This is a margin note.
```

<!-- Below denotes the location of a footnote -->

[^1]


