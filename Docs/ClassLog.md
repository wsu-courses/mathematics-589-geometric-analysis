---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (math-589-2022)
  language: python
  name: math-589-2022
---

Class Log
=========

Sun 23 Jan 2022
---------------
Tested the cookiecutter recipe.
