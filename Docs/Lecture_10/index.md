
# Lecture 10: Area, Coarea, Crofton and Gauss-Bonnet

## Area Formula
Take differentiable $f:\mathbb{R}^n\to\mathbb{R}^n$ and we have 
\begin{gather*}
	\int_{E}Jf\, dx = \int_{f(E)} \mathscr{H}^0(f^{-1}(y))\,dy
\end{gather*}
where the Jacobian $Jf \equiv \sqrt{\det(Df^T\cdot Df)}.$
```{margin}
The zero dimensional measure gives the number of points.
```

So, the integral of the Jacobian is the sum of the number of points in the preimage times the points themselves.

(*Sketch of the situation*)

For $f:\mathbb{R}^n \to \mathbb{R}^m$,
\begin{gather*}
	\int_E Jf\, dx = \int_{f(E)} \mathscr{H}^0(f^{-1}(y))d\mathscr{H}^n(y)
\end{gather*}
In a more general sense,
\begin{gather*}
	\int_E g(x)\, Jf\, dx = \int_{f(E)} \sum_{w\in f^{-1}(y)} g(w)\,d\mathscr{H}^n(y)
\end{gather*}
This holds if $f:M^n \to M^m$, even if $M^n$, $M^m$ are $n$-rectifiable and $m$-rectifiable set or $f$ Lipschitz. 
```{margin}
General principal: "almost everywhere" we consider two functions to be the same if they only differ on a set of measure 0, we are primarily concerned with integrating w.r.t. some measure; we need to be careful because a set may have measure 0 w.r.t. some measure but not another.
```
This is true from Rademacher's theorem, since rectifiable guys are differentiable almost everywhere, and usually when we integrate we can ignore measure zero portions.

```{prf:proof}
Sketch:
If you have a rectifiable set, usually defined as the union of a countable collection of lipschitz images, we can always add null sets (sets of measure 0) and we can restrict ourselves to images of $C^1$ maps. I.e. we can cut out stuff and take unions.
```



## Coarea Formula
Take differentiable $f:\mathbb{R}^n \to \mathbb{R}^m$, $n\geq m$. Then
\begin{gather*}
	\int_E g(x)\, Jf(x)\, dx = \int_{f(E)}\int_{f^{-1}(y)} g(x)\, d\mathscr{H}^{n-m}(x)\, dy
\end{gather*}
```{margin}
Because we are mapping from a higher dimensional space to a lower dimensional space  the inverse images aren't countable. So, to sum them up we need to use an integral.
```
When $n = 2,\ m=1$ this looks something like this:

(*First sketch from coarea*)

These are the level curves of the function on the left and on the right is the range with dots corresponding to each level set. 

We'll focus on the case when $g(x) = 1$. Tangent to the level curve, the Jacobian is $0$, and perpendicular it achieves its maximum. Notice, that $J$ gives the length of the gradient. Also notice that integrating along the gradient in blue from the current level set to the next will give us the height that the function changes in the range. Then we sum over all of the $k$s:
\begin{gather*}
	\sum_k\int_{E_k} Jf(x)\, dx \approx \sum_k\mathscr{H}^1(f^{-1}(y_k))\Delta y_k
\end{gather*}
This encodes the intuitions behind each piece of the coarea formula. 
```{margin}
A useful note is that $\int_{f^{-1}(y)}\, d\mathscr{H}^1(x) = \mathscr{H}^1(f^{-1}(y)$ )
```

This is the same idea as integrating over the whole of space being equivalent to integrating over the sphere, then over all possible radii.

Another example, for $f: \mathbb{R}^n\to\mathbb{R}$ with $f(x)$ being the distance from the origin to $x$, $\nabla f = 1$, so $Jf = 1$. Then integrating $g$ over some set $F$
\begin{gather*}
	\int_F g(x)\, dx = \int_{f(F)}\int_{f^{-1}(y)} g(x)\, d\mathscr{H}^{n-1}(x)\, d\mathscr{H}^1	
\end{gather*}
```{margin}
Paper: [the Curvature of Measures by Federer](https://www.ams.org/journals/tran/1959-093-03/S0002-9947-1959-0110078-1/S0002-9947-1959-0110078-1.pdf)
```

## Crofton's Formula
(*Wigly picture*)

Take projections along the red arrows, and record the number of times the wiggle $\gamma$ is hit (in green). This will create an integer valued function $h$ that depends on the angle of change of our axes $\theta$: $h(\theta)$. For each of these angles we get one of the cyan functions above.

Crofton's formula is then
\begin{gather*}
	\frac{1}{C}\int h(\theta)\, d\theta = \mathscr{H}^1(\gamma),
\end{gather*}
for some $C$ that only depends on dimension. Further, the right hand side is more generally
\begin{gather*}
	\frac{1}{C}\int_{\mathscr{O}}\int_{\mathbb{R}} \mathscr{H}^0(L_x\cap \gamma)\, dx\, do = \mathscr{H}^1,
\end{gather*}
where $\mathscr{O}$ is all of the plains with same dimension as $\gamma$. There's a natural Haar measure on $\mathscr{O}$ that is rotation invariant.

(*Example of the sphere*)

Now we'll consider a proof of this.

```{prf:proof}
Take a segment of our projection line of length $1$
\begin{gather*}
	\frac{1}{4}\int_0^{2\pi} h(\theta)\, d\theta
\end{gather*}

This is now true for any polygonal curve. This can be treated as an approximation of the curve. For a small enough cone around the curve, the sides of the cone will have the same projections of the curve, which is sufficient. It's a bit more hairy for rectifiable sets.
```

A fuller proof can be found in Morgan's book which uses the Coarea formula.

Computational challenge: 10 directions, 2d curve, discrete approximation of Crofton. Further, see if you can take the derivative at each point and see if you can minimize the $\ell_2$ norm.

## Gauss-Bonnet
(*Figure of the shamrock looking thing*)

We take some surface and everywhere we can measure the 2 principle curvatures: $\kappa_1, \kappa_2$. Then
\begin{gather*}
	\kappa_1\kappa_2 = \textrm{Gauss curvature at some point $p$}
\end{gather*}
Gauss-Bonnet says (for 3D)
\begin{gather*}
	\int_E\kappa_1\kappa_2\, dx = 4\pi,
\end{gather*}
assuming we can smoothly transform the surface into the unit sphere.

If we take a unit vector at $p$ and slide it around the surface, we get a map from our surface to the unit sphere.

(*figure cross section of the set in red*)


As we slide the green arrow, how fast are we moving along the unit sphere? If we go around the sphere of radius R we also go around the unit sphere. If we go the length $L$ around the sphere of radius $R$ then we go $L\kappa$ along the unit guy.

Imagine orthogonally there is another guy that we can move in that direction. We then say a little square with size $LK$ on the original surface will go to a little patch on the unit sphere of $KL/R^2$ (not really $R^2, R_1 R_2\ldots$) $LK \to \kappa_1 \kappa_2 LK$

By its action $\kappa_1 \kappa_2$ is acting as the signed Jacobian of the normal map. 

(Figure: one dimensional Gauss-Bonnet)

Instead of $4\pi$ you get $2\pi$ since that's the area of a sphere for 1-d spheres.

Notice that if we move a little normal around the blue guy we get something like the red guy. Every time we trace backwards we have a negative. Starting with the area formula:

Let $J^*$ denote the signed Jacobian

\begin{gather*}
	\int J^* dx = \int_{f(E)}dy
\end{gather*}

Which is nothing but the area, since the image is the whole sphere.

Using degree theory, there's a homotopy (smooth map) from the set to a sphere (or circle) and degree theory says that homotopies preserve degree. Which means that the degree of the sheets around the green unit disk is always 1, so the degree of the set is also 1. 

More generally,
\begin{gather*}
	\int_{M^n} \prod_{i=1}^n \kappa_i\, dx = (n+1)\alpha(n+1)
\end{gather*}
where 
\begin{gather*}
	\alpha(k) \equiv \textrm{volume of the $k$ dimensional unit ball}
\end{gather*}
Neidhem's book has multiple proofs of these results.




