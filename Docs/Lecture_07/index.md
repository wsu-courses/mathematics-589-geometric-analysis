<!-- (Point For Week 7: tbd) -->

# Lecture 7: Manifolds, Vector Fields and Flows: An Invitation to the World of Dynamical Systems

```{figure} Figures/example_figure.png
---
height: 350px
name: ex_fig
---
This is a caption of an example figure
```
----

Here, we'll expand on the ideas about manifolds from the last section.

## Tangent Spaces
---
### With respect to submanifolds
The easiest way to think about tangent spaces is to consider submanifolds (some surface in $\mathbb{R}^n$). 
We pick a point $p$ on the manifold and consider the afine subspace that just touches (is tangent to) the manifold at our point $p$. 

For example, consider a $2D$ surface $M$ embedded in $\mathbb{R}^3$ and a point $p$:
(*Figure 1 of such a surface*)

Every element of the subspace tangent to the manifold is a vector tangent to the manifold. This is usually how we think about tangent spaces. 

---
### With respect to smooth curves
There are, however, other ways of thinking about tangent spaces. Consider the same $2D$ surface as before. 
(*Figure 2 of the surface*)
Consider the family of all smooth curves that touch that manifold at $p$ locally. Each one is some $f_\alpha$, and the patch on the manifold has a coordinate map $\phi$ that brings the local area down into $\mathbb{R}^2$. Then, we can see if $f_\alpha$ is smooth in $\mathbb{R}^2$. So, we consider

\begin{gather*}
	\frac{d}{dt}(\phi\circ f_\alpha)
\end{gather*}

If two $f_a$ have the same tangent at the origin when mapped through $\phi$, we say they're the same function, giving us equivelence classes among $f_\alpha$. This family of equivelence classes froms a vector space (you can go through the details if you'd like).
So, if $f_{\alpha_1} \equiv f_{\alpha_2}$, we know
\begin{gather*}
	\frac{d}{dt}(F\circ f_{\alpha_1})_{t=0}\equiv \frac{d}{dt}(F\circ f_{\alpha_2})_{t=0}.
\end{gather*}
Where $F:M\to \mathbb{R}^2$.

---
### Using directional derivatives 
The last way to construct the tangent space is using the class of directional derivatives. Is the following curve smooth in this context?

(*figure of the sharp guy*)

It could be! In the graph space, it could easily be smooth.

For example, consider a map $\mathbb{R}\to\mathbb{R}^2$ and  suppose the image of our function may look like the green rectangle-like objects, and the graph of the map looks like the blue curve.

(*Figure of the rectangle thing in green*)
(*This may be perfect to be done in manim*)

Clearly, the blue curve is smooth, but when you take the image over all $t$, you'll get the green curve, which does not look smooth.

This kind of thing is very obvious in maps from $\mathbb{R}\to\mathbb{R}$:

(*figure of the 3 maps with the same image*)

All three of these maps have the same image, but are clearly very different.

**Books:** 

So, we can take the operator
\begin{gather*}
	X_p: C^\infty(p) \to \mathbb{R} 
\end{gather*}
Which satisfies
1. $X_p(\alpha f + \beta g) =\alpha X_p(f) + \beta X_p(g)$
2. $X_p(fg) = f(p)X_p(g) + g(p)X_p(f)$ 
Then $X_p$ is a tangent space.
(*this isn't clear...*)
(Pg 106 of Boothby)

There's also the idea of a contact bundle, which we won't explore here.

## Geodesics: Shortest paths
(*This section needs to be fleshed out more*)

We introduce the idea of a connection:

(*Figure of the surface*)

What we'll do is consider moving along a path of constant velocity along our manifold $M$. 
We'll need Lie derivatives. Suppose we have a vector field over our manifold and integral curves:

(*Figure of the surface with the vector field*)

We can compare paths on our manifold by taking subracting tangent vectors. This is done by taking the tangent map
\begin{gather*}
	T\Phi_t(p):T_p \to T_q
\end{gather*}
where $T_p$ is the tangent space at $p$, and look at
\end{gather*}
	\frac{1}{t}(T\Phi_t(p)^{-1}(v) - u).
\end{gather*}
This is the Lie derivative.


Now, consider the manifold:
(*figure of the saddle guy*)
with a path (in red), and take the tangent vector at all points on our path. We're interested in seeing how these vectors are changing. So, we take the second derivative along the path (shown in green, which doesn't live in the tangent space, hence, if it's orthogonal, we can't see the second derivative at all), and projected it into the tangent space at $p$. Now we can define:

A **Geodesic** is a path who's second derivatives are orthogonal to the manifold at every point.

This projection step will also allow us to see how we need to move to make the path shorter.

**Book:** "Visual Differential Geometry" by Needham

A physical representation of geodesics is to take a squash and a thin strip of paper with the center line drawn on it. Picking a pair of points on the squash and laying the strip connecting the two will form a geodesic on the squash!
(*figure of squash*)
Why is this? Consider the normal bundle along our line on the paper. When we lay the sheet on the surface, the normal bundle will remain normal to the curve, which means the curve must be a geodesic.

This gives a nice motivation for viewing geodesics as minimizers of length variation of a curve between two points on our manifold.

---
## Mean Curvature
Consider manifolds that are the boundary of a set.
(*Figure from geometry from derivatives presentation*)

When we look at the change of length by expanding the set by epsilon, we have along the surface 
\begin{gather*}
	\Delta L = 1 + \epsilon\kappa
\end{gather*}
for $1D$ boundary.
This allows us to do the same kind of thing for general sufaces.
(*Formula from the tube formula slide*)

This works as long as our stretch $\epsilon$ is less than the **reach** of the set.
The **reach** of a set is the minimum distance you can shrink the set before the normals touch.
(Why is this relevant?)

In $3D$ we have something similar:
(*Formula from totally tubular slide*)

We interpret $\kappa_1\kappa_2$ (Gaussian Curvature) by the following:
1. Take a manifold that's topo-equiv to the sphere
2. Take it's unit normals
3. Notice that these unit normals are a map from the manifold to the unit sphere
4. Ask "How does the area change under this map?" Particularly, what is the ratio of the areas?
5. This ratio is $\kappa_1\kappa_2$.

(*Figure of the peanut thing*)

The Gauss-Bonnet theorem follows directly from the existence of this map:
\begin{gather*}
	\int_M \kappa_1\kappa_2 \, d\sigma = 4\pi
\end{gather*}
If $M$ is topo-equiv to a sphere.


![](Figures/manifold.png)

```{figure} Figures/mfld_coord_map.png
---
name: ex_fig
---
This is a caption of an example figure
```

```{figure} Figures/mfld_overlap.png
---
name: ex_fig
---
This is a caption of an example figure
```

```{figure} Figures/tan_bundle.png
---
name: ex_fig
---
This is a caption of an example figure
```

```{figure} Figures/tan_planes.png
---
name: ex_fig
---
This is a caption of an example figure
```

```{figure} Figures/velocity_at_p.png
---
name: ex_fig
---
This is a caption of an example figure
```

:::{sidebar} Example Topic

This is an example of a sidebar.
:::

```{margin}
This is a margin note.
```

<!-- Below denotes the location of a footnote -->

[^1]
----

Continuing on the ideas about manifolds from the last section.

## Tangent Spaces
---
### With respect to submanifolds
The easiest way to think about tangent spaces is to consider submanifolds (some surface in $\mathbb{R}^n$). 
We pick a point $p$ on the manifold and consider the afine subspace that just touches (is tangent to) the manifold at our point $p$. 

For example, consider a $2D$ surface $M$ embedded in $\mathbb{R}^3$ and a point $p$:
(*Figure 1 of such a surface*)

Every element of the subspace tangent to the manifold is a vector tangent to the manifold. This is usually how we think about tangent spaces. 

---
### With respect to smooth curves
There are, however, other ways of thinking about tangent spaces. Consider the same $2D$ surface as before. 
(*Figure 2 of the surface*)
Consider the family of all smooth curves that touch that manifold at $p$ locally. Each one is some $f_\alpha$, and the patch on the manifold has a coordinate map $\phi$ that brings the local area down into $\mathbb{R}^2$. Then, we can see if $f_\alpha$ is smooth in $\mathbb{R}^2$. So, we consider

\begin{gather*}
	\frac{d}{dt}(\phi\circ f_\alpha)
\end{gather*}

If two $f_a$ have the same tangent at the origin when mapped through $\phi$, we say they're the same function, giving us equivelence classes among $f_\alpha$. This family of equivelence classes froms a vector space (you can go through the details if you'd like).
So, if $f_{\alpha_1} \equiv f_{\alpha_2}$, we know:

\begin{gather*}
	\frac{d}{dt}(F\circ f_{\alpha_1})_{t=0}\equiv \frac{d}{dt}(F\circ f_{\alpha_2})_{t=0}.
\end{gather*}

Where $F:M\to \mathbb{R}^2$.

---
### Using directional derivatives 
The last way to construct the tangent space is using the class of directional derivatives. Is the following curve smooth in this context?

(*figure of the sharp guy*)

It could be! In the graph space, it could easily be smooth.

For example, consider a map $\mathbb{R}\to\mathbb{R}^2$ and  suppose the image of our function may look like the green rectangle-like objects, and the graph of the map looks like the blue curve.

(*Figure of the rectangle thing in green*)
(*This may be perfect to be done in manim*)

Clearly, the blue curve is smooth, but when you take the image over all $t$, you'll get the green curve, which does not look smooth.

This kind of thing is very obvious in maps from $\mathbb{R}\to\mathbb{R}$:

(*figure of the 3 maps with the same image*)

All three of these maps have the same image, but are clearly very different.

**Books:** 

So, we can take the operator

\begin{gather*}
X_p: C^\infty(p) \to \mathbb{R}
\end{gather*}

Which satisfies
1. $X_p(\alpha f + \beta g) =\alpha X_p(f) + \beta X_p(g)$
2. $X_p(fg) = f(p)X_p(g) + g(p)X_p(f)$ 
Then $X_p$ is a tangent space.
(*this isn't clear...*)
(Pg 106 of Boothby)

There's also the idea of a contact bundle, which we won't explore here.

## Geodesics: Shortest paths
(*This section needs to be fleshed out more*)

We introduce the idea of a connection:

(*Figure of the surface*)

What we'll do is consider moving along a path of constant velocity along our manifold $M$. 
We'll need Lie derivatives. Suppose we have a vector field over our manifold and integral curves:

(*Figure of the surface with the vector field*)

We can compare paths on our manifold by taking subracting tangent vectors. This is done by taking the tangent map

\begin{gather*}
T\Phi_t(p):T_p \to T_q
\end{gather*}

where $T_p$ is the tangent space at $p$, and look at

\begin{gather*}
\frac{1}{t}(T\Phi_t(p)^{-1}(v) - u).
\end{gather*}

This is the Lie derivative.

Now, consider the manifold:
(*figure of the saddle guy*)
with a path (in red), and take the tangent vector at all points on our path. We're interested in seeing how these vectors are changing. So, we take the second derivative along the path (shown in green, which doesn't live in the tangent space, hence, if it's orthogonal, we can't see the second derivative at all), and projected it into the tangent space at $p$. Now we can define:

A **Geodesic** is a path who's second derivatives are orthogonal to the manifold at every point.

This projection step will also allow us to see how we need to move to make the path shorter.

**Book:** "Visual Differential Geometry" by Needham

A physical representation of geodesics is to take a squash and a thin strip of paper with the center line drawn on it. Picking a pair of points on the squash and laying the strip connecting the two will form a geodesic on the squash!
(*figure of squash*)
Why is this? Consider the normal bundle along our line on the paper. When we lay the sheet on the surface, the normal bundle will remain normal to the curve, which means the curve must be a geodesic.

This gives a nice motivation for viewing geodesics as minimizers of length variation of a curve between two points on our manifold.

---
## Mean Curvature
Consider manifolds that are the boundary of a set.
(*Figure from geometry from derivatives presentation*)

When we look at the change of length by expanding the set by epsilon, we have along the surface 

\begin{gather*}
\Delta L = 1 + \epsilon\kappa
\end{gather*}

for $1D$ boundary.
This allows us to do the same kind of thing for general sufaces.
(*Formula from the tube formula slide*)

This works as long as our stretch $\epsilon$ is less than the **reach** of the set.
The **reach** of a set is the minimum distance you can shrink the set before the normals touch.
(Why is this relevant?)

In $3D$ we have something similar:
(*Formula from totally tubular slide*)

We interpret $\kappa_1\kappa_2$ (Gaussian Curvature) by the following:
1. Take a manifold that's topo-equiv to the sphere
2. Take it's unit normals
3. Notice that these unit normals are a map from the manifold to the unit sphere
4. Ask "How does the area change under this map?" Particularly, what is the ratio of the areas?
5. This ratio is $\kappa_1\kappa_2$.

(*Figure of the peanut thing*)

The Gauss-Bonnet theorem follows directly from the existence of this map:

\begin{gather*}
\int_M \kappa_1\kappa_2 \, d\sigma = 4\pi
\end{gather*}

If $M$ is topologically equivalent to a sphere.
