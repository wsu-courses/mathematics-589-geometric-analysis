
# Overview of Course

In this course, I will present advanced insights and tools in a way that requires very little background to understand. More specifically, if you have a strong grasp of calculus, linear algebra and perhaps differential equations, and you are motivated to work a bit to understand some fascinating and useful tools from geometric analysis, you are well prepared for this class.

This is accomplished by exploiting every intuitive handhold to help those without the usual background to scale impressive heights safely. First I will focus on explaining the meaning of theorems and definitions. Next, we will have a look at how they are used. This will be supported by an explanation of the key ideas that make those theorems true or the definitions useful. Finally, if and only if it is clearly useful for understanding and it does not distract from the narrative we are pursuing, a proof will be outlined, using the intuitions we have previously built up. 

There will not be a text for the course, though various notes will be given out from time to time. I will organize a note taking effort and these notes, plus figures should be available soon after each class. We (my group and I) plan on turning the notes into a book that will be published in paperback form, but that will also be given out freely as a pdf. In the notes/book, there will be very carefully chosen references, chosen for the quality of exposition, as well as lists of applications for the ideas and even historical notes. As already mentioned there will be (many) hand drawn figures as well.

The content will focus on (often extremely) economical paths into fairly advanced tools and insghts at the intersection of geometry and analysis. Minimalism not only aids in this ambitious journey, it also forces us to focus on what is important for understanding, for mastery.


 In this course we cover the following list of topics:

* Geometric Linear Algebra (Linear Algebra Done Right According to a Geometric Analyst)
* A Concise Tour of Metric Spaces and other Useful Technical Details
* Spaces: the Zoo of Countries where Everything Happens
* Derivatives = Linear Approximations, and the Huge Zoo of Generalizations
* Outer Measures, Hausdorff Measures, Covering Theorems, and Fractals   
* Inverse Function Theorem, Implicit Function Theorem, and Sards Theorem: Just Three (Powerful) Examples of the Use of Derivatives
* Manifolds, Vector Fields and Flows on Manifolds: An Invitation to the World of Dynamical Systems 
* Degree Theory and Fixed Point Theorems
* Inequalities, Geometrically: Concentration of Measure in High Dimensions, Isoperimetric, Jensens, Cauchy-Schwarz, and AM-GM inequalities
* Area, Coarea, Crofton and Gauss-Bonnet
* Convexity, Legendre-Fenchel Transform, Duality
* Forms, Currents, and Minimal Surfaces: A Bird’s Eye View
* The Flat Norm Saga: from Image Denoising and Shape Recognition through Properties of Minimizers to Median Shapes and Chemical Interfaces
* Smooth, Compact Sets: Distance Functions, Sets With (and Without) Positive Reach, Curvature Measures, and Tube Formulas.
 