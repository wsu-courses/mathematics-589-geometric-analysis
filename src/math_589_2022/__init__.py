"""Math 589 - Geometric Analysis

This package contains source code for the Spring 2022 
offering of Math 589: Topics in Analysis - Geometric Analysis with a View to Use at Washington State University (WSU).
"""
