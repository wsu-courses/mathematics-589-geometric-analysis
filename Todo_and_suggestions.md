# Questions and Answers
* Q: Do we want a section at the end of the book with elementary reference material? Something like a primer on naïve set theory and/or other topics? - Curtis
* Q: I'm thinking we should preserve footnotes for things like in page references/further reading and put everything else in margins/sidebars, anyone have any thoughts on the matter? - Curtis

# General Book
* I went and added all the titles from the notes-for-589 document to lectures 5-14, I think said titles are too long to render properly and so they do not appear in the contents - need to either shorten the names or find another workaround eventually - Curtis (update: seems to be rendering correctly for the moment)

# Lecture 1

* 
* 
* 


# Lecture 2

* Redraw Figures - Curtis
* Possibly reorder material to make more sense - Curtis
* Prove the sky is blue - Curtis
* Replace $\forall$s and $\exists$s with text? - Curtis
* Figure out the numbering on theorems and whether counter persists between chapters and if there is a way to link to a theorem in a chapter / to a theorem in a previous chapter - Curtis
* Update title image placeholder? Current one is generated from some LaTeX code I found on stack overflow - Curtis

# Lecture 3

* 
* 
* 



# Lecture 4

* 
* 

# Lecture 5

* Rough transcript taken live, needs to be edited - Curtis
* Fix LaTeX errors, in particular double subscript - Curtis
* Divide and break up content into sidebars/margins as needed - Curtis
* Add some key definitions and flesh out some proofs - Curtis
* Add temp figures from Dr. Vixie's notes after he uploads them - Curtis
* 

# Lecture 7

* Figures/mfld_coord_map.png, Figures/mfld_overlap.png, Figures/tan_bundle.png, Figures/tan_planes.png, and Figures/velocity_at_p.png were made by Richie Dadhley richie@dadhley.com for his notes on a course on general relativity here: https://docs.wixstatic.com/ugd/6b203f_dc24fe06fbe14a71ae32a1ad031e1928.pdf?index=true he has given me the LaTeX for these notes and said we can use any of his figures without attributing him

